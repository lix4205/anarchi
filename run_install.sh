#/bin/bash

# Define alias in /bin
BINARY="dist-install"
# Define python script name
BINARY_PYTHON="dist_conf.py"

source "$(dirname "$(realpath $0)")/../../bash/run_install.sh" "${@}"
