# ANother ARCH Installer (not just arch...)

This program was originally created to restitute command lines to be executed for a new archlinux installation.
Quickly it has been a way to install "easyly" a new (low-level*) linux distribution with a desktop (or not).

You have to mount your partition yourself before installation (NFS installation (PXE) are available !).

It install archlinux by default (add --debian option else).

There are many options to add to the command line (type bash run_install.sh --help for more information).
If no option specifeid, it cover essentials questions (/!\ except GRUB**) for a  linux "standard" operating system installation (desktop, network, localisation, main user info,hostname,...).


\* I mean distribution like archlinux, debian or gentoo

** GRUB can be installed via "--loader path_disk" option (BIOS only !)

## Get started 

### standard installation
```
git clone https://gitlab.com/lix4205/anarchi 
cd anarchi
git submodule init
git submodule update
# Or (short version...)
# git clone --recurse-submodules https://gitlab.com/lix4205/anarchi  && cd anarchi

bash run_install.sh [-l /dev/sdX] /path/to/your/new/system/installation
```
