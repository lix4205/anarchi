# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# coding: utf-8
import subprocess
from lib import system_conf

class graphic_env :
    def __init__(self):
        self.RACINE = ""
        # Graphic drivers
        self.VID_DRIVERS = []
        self.VID_DRIVERS_IDX = -1
        self.VID_DRIVERS_LIB = "" 
        ## Desktop Environnement
        self.DESKTOP_ENV = []
        self.DESKTOP_ENV_IDX = -1
        self.DESKTOP_ENV_LIB = ""
        
        self.DISPLAYMANAGER = []
        self.DISPLAYMANAGER_IDX = -1
        self.DISPLAYMANAGER_LIB = ""
        
        #self.INPUT_ENV = ""
        #self.LOCALE = []
        #self.LOCALE_IDX = -1
        ## Keyboard map
        #self.KBDMAP = []
        #self.KBDMAP_IDX = -1
        ## Keyboard map in X
        #self.XKBDMAP = []
        #self.XKBDMAP_IDX = -1
    def GetDesktop(self) :
        if self.DESKTOP_ENV_IDX > -1 :
            return self.DESKTOP_ENV[self.DESKTOP_ENV_IDX]
    
    def GetGraphicDriver(self, *arg_cmd) :
        self.VID_DRIVERS_IDX = self.graphic_setting(*arg_cmd)

    def graphic_setting(self, yy, arg_cmd = "") :
        # On indique le header JSON
        MENU_OPT =    {'quit': True, '0': "(0 infos matos)", "prec" : True }
        MENU_OPT_NV =    {'quit': True, "prec" : True }
            # Get network interfaces
        self.VID_DRIVERS = system_conf.EN_GRAPHICS.GetArray()
        #print(IFACES)
        # list_if send list like "dhcpcd@inet" 
        #i = 0
        if arg_cmd != "" : 
            if arg_cmd in system_conf.EN_GRAPHICS.NVIDIA304 :
                self.VID_DRIVERS = [ *system_conf.EN_GRAPHICS.GetArrayNvidia()  ]
                return 0
            elif arg_cmd in system_conf.EN_GRAPHICS.NVIDIA340 :
                self.VID_DRIVERS = [ *system_conf.EN_GRAPHICS.GetArrayNvidia()  ]
                return 1
            elif arg_cmd in system_conf.EN_GRAPHICS.NVIDIA :
                yo = yy.rid_menu( "Choisir", MENU_OPT_NV, *system_conf.EN_GRAPHICS.GetArrayNvidia())
                if yo > 0 :
                    #yo += len(VID_DRIVERS)
                    self.VID_DRIVERS = [ *system_conf.EN_GRAPHICS.GetArrayNvidia()  ]
                    return yo - 1
                #else :
                    #return -1
            elif arg_cmd == system_conf.EN_GRAPHICS.AMD :
                return 0
            elif arg_cmd == system_conf.EN_GRAPHICS.RADEON :
                return 1
            elif arg_cmd in system_conf.EN_GRAPHICS.NOUVEAU:
                return 2
            elif arg_cmd == system_conf.EN_GRAPHICS.INTEL:
                return 3
            elif arg_cmd in system_conf.EN_GRAPHICS.VB:
                return 5
            if arg_cmd == "none" :
                return -1
            #elif arg_cmd == system_conf.EN_GRAPHICS.NOTHING:
                #return 5
        
        yo = yy.rid_menu("Choisir", MENU_OPT, *self.VID_DRIVERS)
        if yo == 0 :
            output = subprocess.check_output(["lspci | grep --color VGA" ], shell=True)
            print(output.decode())
            yo = yy.rid_menu("Choisir (0 infos matos)", MENU_OPT, *self.VID_DRIVERS)
        if yo == len(self.VID_DRIVERS) :
            return - 1 
        if yo == 5 :
            yo = yy.rid_menu("Choisir", MENU_OPT_NV, *system_conf.EN_GRAPHICS.GetArrayNvidia())
            if yo > 0 :
                yo += len(self.VID_DRIVERS)
                self.VID_DRIVERS = [ *self.VID_DRIVERS, *system_conf.EN_GRAPHICS.GetArrayNvidia()  ]
                return yo - 1
            else :
                return graphic_setting()
        
        if yo > 0 :
            return yo - 1
        else :
            return -1
        
    def graphic_setting_nv(self) :
        yo = yy.rid_menu( "Choisir", "q", *system_conf.EN_GRAPHICS.GetArrayNvidia())
        if yo > 0 :
            return yo
        else :
            return graphic_setting()
    
    # Environnement de bureau
    def GetEnvironnement(self, user_inputs, arg_cmd = "") :
        #print(self.DISPLAYMANAGER_LIB)
        MENU_OPT =    {'quit': True, '0': " a graphic driver", "prec" : True }
        
        
        self.DESKTOP_ENV = system_conf.EN_ENVIRONNEMENT.GetArray()

        if arg_cmd in self.DESKTOP_ENV :
            self.DESKTOP_ENV = [arg_cmd]
            self.DESKTOP_ENV_IDX = 0
        else :
            yo = user_inputs.rid_menu("Choisir", MENU_OPT, *self.DESKTOP_ENV)
        
            if yo > 0 :
                self.DESKTOP_ENV_IDX = yo - 1
        # Get display manager
        self.GetDisplayManager(user_inputs, self.DISPLAYMANAGER_LIB)
      
    
    def GetDisplayManager(self, user_inputs, arg_cmd = "") :
        MENU_OPT =    {'quit': True}

        self.DISPLAYMANAGER = system_conf.EN_DISPLAYMANAGER.GetArray()
            
        if arg_cmd in self.DISPLAYMANAGER :
            self.DISPLAYMANAGER = [arg_cmd]
            self.DISPLAYMANAGER_IDX = 0
            return 0
        
        
        if self.DESKTOP_ENV_IDX > -1 :
            #print(self.GetDesktop())
            if user_inputs.rid_continue("Choix DM {}", system_conf.EN_ENVIRONNEMENT.GetArrayDM()[self.GetDesktop()]) :
                self.DISPLAYMANAGER_IDX = 0
                self.DISPLAYMANAGER = [system_conf.EN_ENVIRONNEMENT.GetArrayDM()[self.GetDesktop()]]
            else :            
                choix_dm = user_inputs.rid_menu("Choisir", MENU_OPT, *self.DISPLAYMANAGER)
                
                if choix_dm > 0 :
                    self.DISPLAYMANAGER_IDX = choix_dm - 1
                    return choix_dm - 1
                else :
                    return -1


   
