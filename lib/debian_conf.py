# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# coding: utf-8
#import subprocess

import os
import subprocess
from multiprocessing.pool import ThreadPool
from lib.user_conf import user_conf
from lib import chroot
from lib.system_conf import DistConf

class debian_conf(user_conf) :
    PACK_MANAGER="apt"
    # #PACK_MANAGER_OPTS=""
    # PACK_MANAGER_SEARCH = " search"
    # PACK_MANAGER_INSTALL = " install"  #
    # PACK_MANAGER_UPDATE = " update"
    # PACK_MANAGER_UPGRADE = " upgrade"
    # PACK_MANAGER_NOCONFIRM = "-y"
    PACK_MANAGER_CACHE_DEFAULT = "/var/cache/apt/archives"
    DEBIAN_MIRROR_DEFAULT = "http://ftp.fr.debian.org/debian"
    DEBIAN_SERVER_SECURITY_DEFAULT = "http://security.debian.org/"
    
    KERNEL_ARCH = {
        "i386" : "686",
        "amd64":"amd64"
        }

    GRUB_BIN="/usr/sbin/update-grub"
    
    DEBOOTSTRAP_VERSION = "1.0.114"
    DEBIAN_ARCH_INSTALL_SCRIPTS_URL = "http://snapshot.debian.org/archive/debian/20191116T030727Z/pool/main/a/arch-install-scripts/arch-install-scripts_23-1_all.deb"
    DEBIAN_MAIN = "main"
    DEBIAN_CONTRIB = "contrib"
    DEBIAN_SOURCE_MAIN = "{} {} {}"
    DEBIAN_SOURCE_SECURITY = "{} {}/updates {}"
    #DEBIAN_SOURCE_MAIN = "deb {} {} {}\ndeb-src {} {} {} "
    #DEBIAN_SOURCE_SECURITY = "deb {} {}/updates {}\ndeb-src {} {}/updates {}"
    
	#exe ">" "$RACINE/etc/apt/sources.list" echo -e "deb $DEBIAN_SERVER $DEBIAN_VERS main contrib\ndeb-src $DEBIAN_SERVER $DEBIAN_VERS main\ndeb $DEBIAN_SERVER_SECURITY $DEBIAN_VERS/updates main\ndeb-src $DEBIAN_SERVER_SECURITY $DEBIAN_VERS/updates main" 

    user_conf.KERNEL_UPDATE = "/usr/sbin/mkinitramfs -o /boot/initrd.img-{} {}"

    # # user_conf.HOOKS_LUKS_MKINITCPIO = [ "keyboard keymap consolefont", "encrypt" ]
    # #     "sed -i \"s/^MODULES=.*/MODULES=netboot/\" \"/etc/initramfs-tools/initramfs.conf\"",
    # # user_conf.CMD_LUKS_MKINITCPIO = "sed -i \"s/\(HOOKS=(.*kms\)\(.*block\) /\\1 " + HOOKS_LUKS_MKINITCPIO[0]+ "\\2 " + HOOKS_LUKS_MKINITCPIO[1] + " /g\" {}/etc/mkinitcpio.conf"

    DEBIAN_CMD = {
        # Creation racine
        "create_root" : "mkdir -m 0755 -p \"{}/var/cache/pacman/pkg\" \"{}/var/lib/pacman\" \"{}/var/log\" \"{}/dev\" \"{}/run\" \"{}/etc\"", 
        "create_tmp" : "mkdir -m 1777 -p \"{}/tmp\"", 
        "create_proc" : "mkdir -m 0555 -p \"{}/sys\" \"{}/proc\"", 
        # Installation de base
        #"pack_update" : PACK_MANAGER + " -r {} " + PACK_MANAGER_UPDATE, #. format("{}/var/{{cache/pacman/pkg,lib/pacman,log}}", "{}/{{dev,run,etc}}"), 
        "pack_install_base" : "debootstrap" + " {} {} --arch {} {} {} {}", 
    
    
        #if !  exe $QUIET debootstrap --include=$PACKAGES_PLUS --arch ${real_arch[db_$ARCH]} $DEBIAN_VERS $RACINE $DEBIAN_MIRROR; then

    #"copy_gnupg" : "cp -a /etc/pacman.d/gnupg {}/etc/pacman.d/", 
        #"copy_mirrorlist" : "cp -a /etc/pacman.d/mirrorlist {}/etc/pacman.d/", 
        
        # System configuration 
        # Hosts/hostname
        "hostname" : "echo \"{}\" > \"{}/etc/hostname\"", 
        "hosts" : "echo -e \"" + user_conf.HOSTS_ENTRY +"\" > \"{}/etc/hosts\"",
        # Locale
        #"locale" : "echo LANG=\"{}\" > \"{}/etc/locale.conf\"", 
        "localeset" : "sed -i \"s/\# {}/{}/g\" /etc/locale.gen", 
        # Keyboard console keymap
        #"keymap" : "echo KEYMAP={} > \"{}/etc/vconsole.conf\"", 
        # Timezone
        "timezone" : "ln -fs /usr/share/zoneinfo/{} /etc/localtime", 
        #"timezone_remove" : "rm {}/etc/localtime",
        # We have to write those 2 commands in a file to 
        "path_debian" : "export PATH=$PATH:/bin:/sbin:/usr/sbin", 
        "dpkg_kbdloc" : "dpkg-reconfigure locales keyboard-configuration", 
        
        # Install optional packages
        "pack_sources" : "echo -e \"{}\" > {}/etc/apt/sources.list",
        "pack_dl" : PACK_MANAGER + " {} --no-install-recommends -d " + DistConf.PACK_MANAGER[PACK_MANAGER]["install"] + " {}",
        "pack_install" : PACK_MANAGER + " {} --no-install-recommends " + DistConf.PACK_MANAGER[PACK_MANAGER]["install"] + " {}",
        
        # Generate fstab
        "genfstab_begin" : "echo -e \"" + user_conf.FSTAB_BEGIN + "\" > \"{}/etc/fstab\"",
        "genfstab" : "genfstab -U \"{}\" > \"{}/etc/fstab\"", 
        # Force X to use these keymap
        "xkeyboard" :  "mkdir -p {}/etc/X11/xorg.conf.d/ && echo -e \"" + user_conf.KEYBOARD_CONF +"\" > \"{}/etc/X11/xorg.conf.d/00-keyboard.conf\""
    }
    #"multilib" : ("echo -e {}", "{}/etc/pacman.conf"),   }

    CMD_NFSROOT = [
        "sed -i \"s/^MODULES=.*/MODULES=netboot/\" \"/etc/initramfs-tools/initramfs.conf\"", 
        "echo -e \"{}\" >> {}/etc/initramfs-tools/modules", 
        user_conf.KERNEL_UPDATE
    ]
        
    #KERNEL_NFS="${KERNEL//linux-image-/}"
#     KERNEL_NFS=${KERNEL_NFS//-$REAL_ARCH/}
    
    #def __init__(self):
        #super().__init__() 
        #self.name = ""
        #self.sudo = False
        #self.hostname = ""
        #self.DISTRIB = "arch"
       
    def __init__(self, user_input, conf_user, local, graphic, reseau):
        # super().__init__(local, graphic, reseau, conf_user.GRUB_INSTALL)
        super().__init__(local, graphic, reseau, conf_user.GRUB_INSTALL, conf_user.LUKS_PART, conf_user.GRUB_EFI)
        self.PATH_INSTALL = conf_user.PATH_INSTALL 
        self.USER_NAME = conf_user.USER_NAME
        self.USER_PASSWD= conf_user.USER_PASSWD
        self.ROOT_PASSWD= conf_user.ROOT_PASSWD
        self.SUDO = conf_user.SUDO
        self.hostname = conf_user.hostname
        self.DISTRIB = conf_user.DISTRIB 
        self.NEW_ARCH = conf_user.NEW_ARCH 
        self.SUDO_SET = conf_user.SUDO_SET
        
        
        self.RAM_INSTALL = conf_user.RAM_INSTALL
        self.QUIET = conf_user.QUIET
        self.TEST_MODE = conf_user.TEST_MODE
        self.INTERACTIVE = conf_user.INTERACTIVE
        self.PACKAGES += conf_user.PACKAGES

        if self.INTERACTIVE :
            self.PACK_MANAGER_NOCONFIRM == ""
            
        # Get classes to manage user IO
        self.INPUTS = user_input
        self.MIRROR = debian_conf.DEBIAN_MIRROR_DEFAULT
        
        #self.DebootstrapVersion()

        # pacman = self.find_pack_manager()
        self.SetPackManager(self.find_pack_manager())
        self.PACK_MANAGER_CACHE_PKG = conf_user.PACK_MANAGER_CACHE_PKG

        # print(DistConf.PACK_MANAGER)
        # if pacman != "" :
        # else :
            # self.PACK_MANAGER = debian_conf.PACK_MANAGER
        self.prerequis = "debootstrap"

        self.DEBIAN_VERS = "bookworm"
        self.REAL_ROOT = self.PATH_INSTALL
        conf_user.REAL_ROOT = self.PATH_INSTALL
        user_input.msg_n("Checking {}", "32", "32", "environnement")
        if not self.exist_install(user_input, self.prerequis, self.prerequis) :
            self.INPUTS.die("Pas de {}, pas de {} !", self.prerequis, "chocolat")


        self.DOWNLOAD_OK = True
        self.file_zoneinfo = "/usr/share/zoneinfo/zone.tab"
        if not os.path.exists("/usr/share/kbd/keymaps") :
            if not os.path.exists("/usr/share/keymaps") :
                user_input.die("Le dossier {} n'est pas présent sur le système.", "/usr/share/keymaps")
            self.locale.file_kbdmap = "/usr/share/keymaps"
            self.locale.file_kbdmapext = "kmap.gz"
        self.file_locale = "/etc/locale.gen"
        #self.file_xkbdmap = ""
        self.DEBIANVERSIONS=user_conf.DEBIANVERSIONS

        self.debian_version()

        self.LOG_EXE="/tmp/anarchi.log"
        self.FILE_COMMANDS="/tmp/anarchi_command"
        self.SYSTEMD_ENABLE = "" #user_conf.GetPackagesFromKeys("arch", "systemd_enable")
        
        self.AddPackages()
        #if self.GRUB_INSTALL != "" :
            #self.PACK_MANAGER_INCLUDE +=  "," + user_conf.GetPackagesFromKeys("debian", "pack_grub").strip()
            
        #self.PACKAGES_AUTO += self.GetPackages("pack_nav")
        #self.PACKAGES_AUTO += self.GetPackages("pack_common")
        #self.PACKAGES_AUTO += self.GetPackages("pack_desktop")
        
        self.OVERRIDE_PATH = debian_conf.DEBIAN_CMD["path_debian"]

    def SetPackManager(self, pacman) :
        self.PACK_MANAGER = DistConf.PACK_MANAGER[pacman]
        self.PACK_MANAGER_NOCONFIRM = self.PACK_MANAGER["noconfirm"]
        self.PACK_MANAGER_SEARCH = pacman  + " " + self.PACK_MANAGER["search"]
        self.PACK_MANAGER_INSTALL = pacman + " " + self.PACK_MANAGER_NOCONFIRM + " " + self.PACK_MANAGER["install"]
        self.PACK_MANAGER_UPDATE = pacman + " " + self.PACK_MANAGER["update"]
        self.PACK_MANAGER_UPGRADE = pacman + " " + self.PACK_MANAGER_NOCONFIRM + " " + self.PACK_MANAGER["upgrade"]
        self.PACK_MANAGER_CACHE_DEFAULT="/var/cache/apt/archives"
        self.PACK_MANAGER_INCLUDE = "--include=locales,console-setup,console-data,kbd"
    
    def debian_version(self) :
        l_s_vers = ""
        if len(self.DISTRIB.split("|")) > 1 :
            l_s_vers = self.DISTRIB.split("|")[1]
        
        if l_s_vers in self.DEBIANVERSIONS :
            self.DISTRIB = "debian"
            self.DEBIAN_VERS = l_s_vers
            return 1

        if not self.INPUTS.rid_continue("Install : {}", self.DEBIAN_VERS ) :
            yo = self.INPUTS.rid_menu( "Choisir debian", {"quit" : True}, *self.DEBIANVERSIONS)
            self.DEBIAN_VERS = self.DEBIANVERSIONS[yo - 1]
        
        self.DISTRIB = "debian"
           
    def next_cmd(self) :
        str2return = ""
        if self.NEW_ARCH != "" :
            str2return += " --arch " + self.NEW_ARCH 
        
        return str2return + " --" + self.DEBIAN_VERS + " " + super().next_cmd()

    def DebootstrapVersion(self) :
        #output = subprocess.check_output(["debootstrap --version" ], shell=True)
        output = self.running.check_output("debootstrap --version")
        version = ""
        for s in output.decode().split(" ")[1] :
            if s.isdigit() or s == ".":
                version += s
            else :
                break

        if int(version.split(".")[2]) < int(debian_conf.DEBOOTSTRAP_VERSION.split(".")[2]) :
            return False
        return True
        
    def GetKernelFromApt(self) :
        if self.TEST_MODE :
            return "linux-image"
        
        CMD_CHECK = "chroot \"" + self.PATH_INSTALL + "\" apt search linux-image-[0-9].*[0-9]-" + debian_conf.KERNEL_ARCH[self.NEW_ARCH] + " | grep ^linux-image-[0-9].*[0-9]-" + debian_conf.KERNEL_ARCH[self.NEW_ARCH] + " | grep -vE \"\-dbg|\-unsigned\" | sed \"s/\(.*\)\/.*/\\1/g\" | head -n 1"
        #output = subprocess.check_output(["chroot \"" + self.PATH_INSTALL + "\" apt search linux-image-[0-9].*-[0-9]-" + debian_conf.KERNEL_ARCH[self.NEW_ARCH] + " | grep ^linux-image-[0-9].*-[0-9]-" + debian_conf.KERNEL_ARCH[self.NEW_ARCH] + " | sed \"s/\(.*\)\/.*/\\1/g\" | head -n 1" ], shell=True)
        # print(CMD_CHECK)
        output = self.running.check_output(CMD_CHECK)
        result = output.decode().strip()
        
        if result != "" :
            print(self.INPUTS.out_n("", "(Kernel {} found)", "32", "32", result), end=" ")
            return result
        else :
            self.INPUTS.clear_line()
            self.INPUTS.die("No kernel found !\n")
            #print()
    
    def SetSourcesList(self) :
        # Main and contrib
        src_list = "deb " + debian_conf.DEBIAN_SOURCE_MAIN.format(self.MIRROR, self.DEBIAN_VERS, debian_conf.DEBIAN_MAIN + " " + debian_conf.DEBIAN_CONTRIB)
        src_list += "\ndeb-src " + debian_conf.DEBIAN_SOURCE_MAIN.format(self.MIRROR, self.DEBIAN_VERS, debian_conf.DEBIAN_MAIN )
        # security
        src_list += "\ndeb " + debian_conf.DEBIAN_SOURCE_MAIN.format(debian_conf.DEBIAN_SERVER_SECURITY_DEFAULT, self.DEBIAN_VERS + "/updates", debian_conf.DEBIAN_MAIN)
        src_list += "\ndeb-src " + debian_conf.DEBIAN_SOURCE_MAIN.format(debian_conf.DEBIAN_SERVER_SECURITY_DEFAULT, self.DEBIAN_VERS + "/updates", debian_conf.DEBIAN_MAIN)
        
        return src_list
    
    def anarchi_pre_install(self) :
        if self.GetNetworkConfig() != "nfsroot" and not self.check_command("genfstab") :
            #print("saky", end = "")
            if self.INPUTS.rid_continue("Installer {} ? (Nécéssaire pour genfstab)", "arch-install-scripts") :

                ThreadPool(1).imap_unordered(self.download_img, [("/tmp/arch-install-scripts.deb" ,debian_conf.DEBIAN_ARCH_INSTALL_SCRIPTS_URL)])

                self.WaitForDownload(self.INPUTS, False, "En attente {}...", "arch-install-scripts")
                CMD_CREATE = "dpkg -i /tmp/arch-install-scripts.deb"
                if not chroot.run_command.run(CMD_CREATE) == 0 :
                    self.INPUTS.die("Pas de {}, pas de {} !", "genfstab", "chocolat")
                    
        if self.PACK_MANAGER_CACHE_PKG != "" and not self.DebootstrapVersion():
            self.running.add("mkdir -p {}".format(self.PATH_INSTALL + self.PACK_MANAGER_CACHE_DEFAULT))
            self.running.add("cp -n \"{}\"/* \"{}\"".format(self.PACK_MANAGER_CACHE_PKG, self.PATH_INSTALL + self.PACK_MANAGER_CACHE_DEFAULT))

        return self.running.run_all_once(self.anarchi_pre_install.__name__) == 0

    # Install base system
    def anarchi_base(self) :
        RACINE = self.PATH_INSTALL

        # TODO Mount packages cache !!!
        if self.PACK_MANAGER_CACHE_PKG != "" and self.DebootstrapVersion():
            self.PACK_MANAGER_OPTS = "--cache-dir " + self.PACK_MANAGER_CACHE_PKG 
        
        if self.SUDO :
            self.PACK_MANAGER_INCLUDE += ",sudo"
            
        CMD_CREATE = debian_conf.DEBIAN_CMD["pack_install_base"].format(self.PACK_MANAGER_INCLUDE, self.PACK_MANAGER_OPTS, self.NEW_ARCH, self.DEBIAN_VERS, RACINE, self.MIRROR)
        self.running.add(CMD_CREATE)
        
        res_cmd = self.running.run_all_once( self.anarchi_base.__name__ )
        print(res_cmd)
        if res_cmd == 0 :
            if self.PACK_MANAGER_CACHE_PKG != "" :
                chroot.run_command.run("mv -n \"{}\"/* \"{}\"".format(self.PATH_INSTALL + self.PACK_MANAGER_CACHE_DEFAULT, self.PACK_MANAGER_CACHE_PKG))
                self.running.add_mount(self.PATH_INSTALL + self.PACK_MANAGER_CACHE_DEFAULT, self.PACK_MANAGER_CACHE_PKG, "none", "bind") 
                
            if not self.running.Init() :
                self.INPUTS.die("Error chroot config !")
                
        # pacman = debian_conf.PACK_MANAGER
        self.SetPackManager(debian_conf.PACK_MANAGER)
        return res_cmd == 0

    def anarchi_conf(self) :
        res_cmd = 0
        quiet_mode = chroot.run_command.QUIET_MODE
        try :
            CMD_CREATE = debian_conf.DEBIAN_CMD["hostname"].format(self.hostname, self.PATH_INSTALL)
            self.running.add(CMD_CREATE)
            CMD_CREATE = debian_conf.DEBIAN_CMD["hosts"].format(self.hostname, self.hostname, self.PATH_INSTALL)
            self.running.add(CMD_CREATE)

            # Modification de la locale du systeme dans le fichier /etc/locale.conf
            self.running.add_chroot(debian_conf.DEBIAN_CMD["localeset"].format(self.GetLocale(), self.GetLocale()), self.REAL_ROOT)
            ## Suppression de la timezone courante (UTC) 
            #self.running.add(debian_conf.ARCH_CMD["timezone_remove"].format(self.PATH_INSTALL))
            # Création du lien vers la bonne timezone dans : /usr/share/zoneinfo/
            self.running.add_chroot(debian_conf.DEBIAN_CMD["timezone"].format(self.GetTimeZone()), self.REAL_ROOT)
            # Execution de dpkg pour reconfigurer les locales et le codage clavier.
            # TODO : Gerer mode quiet.
            # run dpkg to set locales and keyboard configuration
            self.running.add_chroot(user_conf.USER_CMD_CHROOT.format("bash", debian_conf.DEBIAN_CMD["path_debian"] + "\n" + debian_conf.DEBIAN_CMD["dpkg_kbdloc"]))
            #self.SetLang(running)
            
            # TODO FSTAB : Gerer NFS root !    
            if self.GetNetworkConfig() != "nfsroot" :
                #if os.path.exists("/usr/sbin/genfstab") :
                CMD_CREATE = debian_conf.DEBIAN_CMD["genfstab"].format(self.PATH_INSTALL, self.PATH_INSTALL)
                self.running.add(CMD_CREATE)
                
            # Disable quiet mode to get ncurse screen display correctly 
            if quiet_mode :
                chroot.run_command.QUIET_MODE = False
            res_cmd = self.running.run_all_once( self.anarchi_conf.__name__ ) == 0
            chroot.run_command.QUIET_MODE = quiet_mode

            return res_cmd
        except Exception as e :
            print("Error come from {}"".\n" + e)
            return False
          
    def anarchi_packages(self) :
        RACINE = self.PATH_INSTALL
        CHROOT_PARAM = ""

        CHROOT_PARAM = "chroot \"" + self.PATH_INSTALL + "\" " 
            
        res_pak = self.running.add(debian_conf.DEBIAN_CMD["pack_sources"].format(self.SetSourcesList(), self.REAL_ROOT))
        
        kernel = self.GetKernelFromApt()
        
        res_pak = self.running.add_chroot(debian_conf.DEBIAN_CMD["pack_dl"].format(self.PACK_MANAGER_NOCONFIRM, self.PACKAGES_AUTO.strip()  + " " + self.PACKAGES.strip() + " " + kernel), self.PATH_INSTALL)

        
        res_pak = self.running.add_chroot(debian_conf.DEBIAN_CMD["pack_install"].format(self.PACK_MANAGER_NOCONFIRM, self.PACKAGES_AUTO.strip()  + " " + self.PACKAGES.strip() + " " + kernel), self.PATH_INSTALL)
    
        # Creation du fichier /etc/X11/xorg.conf.d/00-keyboard.conf
        if self.graphic.DESKTOP_ENV_IDX > -2 :
            CMD_CREATE = debian_conf.DEBIAN_CMD["xkeyboard"].format(self.PATH_INSTALL, self.GetXKbdmap().split("_")[0], self.PATH_INSTALL)
            res_cmd = self.running.add(CMD_CREATE)
            
        # Kernel compilation
        self.KERNEL = kernel
        # self.running.add_chroot(user_conf.KERNEL_UPDATE.format(kernel, kernel))
        return self.running.run_all_once(self.anarchi_packages.__name__) == 0
        
    def anarchi_nfs_root(self) :
        kernel = self.KERNEL.replace("linux-image-", "")
        debian_conf.CMD_NFSROOT[1] = debian_conf.CMD_NFSROOT[1].format(self.LIST_MODULES.replace(" ", "\n"), self.PATH_INSTALL)
        debian_conf.CMD_NFSROOT[2] = debian_conf.CMD_NFSROOT[2].format(kernel, kernel)

        return user_conf.anarchi_nfs_root(self, debian_conf.CMD_NFSROOT)

    def anarchi_luks(self) :
        # # GRUB config
        # double_quote_escape = "\"".replace("\"", "\\\"")
        # cmd_luks = user_conf.GRUB_CMD["luks_config"].format(double_quote_escape + "cryptdevice=UUID={}:root-{} root=\/dev\/mapper\/root-{}".format(self.UUID_ROOT, self.hostname, self.hostname) + double_quote_escape, self.PATH_INSTALL)
        # self.running.add(cmd_luks)

        # print(self.KERNEL)
        super().anarchi_luks()
        kernel = self.KERNEL.replace("linux-image-", "")
        # # mkinitcpio config
        # cmd_luks = user_conf.CMD_LUKS_MKINITCPIO.format(self.PATH_INSTALL)
        # # print(cmd_luks)
        # self.running.add(cmd_luks)
        # Kernel update
        self.running.add_chroot(user_conf.KERNEL_UPDATE.format(kernel, kernel))

        return self.running.run_all_once( self.anarchi_luks.__name__ ) == 0

    def install(self, todel = "") :
        try :
            self.REAL_ROOT = self.PATH_INSTALL
            
                        
            self.running = chroot.chroot_init(self.PATH_INSTALL, self.TEST_MODE, self.QUIET)

            self.run_install(self.anarchi_pre_install,
                                            ("Preparing {} install", "debian"),
                                            ("Ready to install {} !", "debian"),
                                            ("Error occured while preparing {} installation !", "debian"))
                
            user_conf.install(self)
        except Exception as e :
            #chroot.chroot_init.umount_disk()
            self.INPUTS.error("Error come from {}"".\n" + e, self.__name__)
