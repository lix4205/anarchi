# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# coding: utf-8
import subprocess

class EN_NETCONF :
    NETWORKMANAGER = ["networkmanager", "NetworkManager", "nm"]
    CONNMAN = "connman"
    DHCPCD = "dhcpcd"
    NFSROOT = "nfsroot"
    NOTHING = "Rien (TODO...)"
    #"NetworkManager", "Connman", "Dhcpcd", "Nfsroot (PXE)", "Rien (TODO...)"
    def GetArray() :
        return [ EN_NETCONF.NETWORKMANAGER[1], EN_NETCONF.CONNMAN.capitalize(), EN_NETCONF.DHCPCD.capitalize(), EN_NETCONF.NFSROOT.capitalize() + " (PXE)", EN_NETCONF.NOTHING]
    
    def GetArrayCli() :
        return [ EN_NETCONF.NETWORKMANAGER[1], EN_NETCONF.CONNMAN.capitalize(), EN_NETCONF.DHCPCD.capitalize(), EN_NETCONF.NFSROOT.capitalize() + " (PXE)", EN_NETCONF.NOTHING]
    # List network interfaces with command line lspci for Ethernet or ip for wireless...
    # USB Ethernet devices should not be recognised, and also, wifi devices named "wlanX" should not work either on new installed system...
    def list_if() :
        IFACES = list()
        # Get ethernet interfaces throught lspci
        #output = subprocess.run(["echo \"entre ?\";read truc; echo $truc >> /tmp/test_redirect" ], shell=True)
        output = subprocess.check_output(["lspci | grep Ethernet | awk '{print $1}'" ], shell=True)
        result = output.decode().strip().split('\n')
        for iface in result :
            if iface != "" :
                IFACES.append("dhcpcd@enp" + str(int(iface.split(":")[0], 16)) + "s" + str(int(iface.split(":")[1].split(".")[0], 16)))
            #print("enp" + str(int(iface.split(":")[0], 16)) + "s" + str(int(iface.split(":")[1].split(".")[0], 16)))
        
        # TODO : Check this issue
        ## Get wifi interfaces throught ip addr
        output = subprocess.check_output(['ip addr | grep "^[0-9]: w" | sed "s/^.*: \\(.*\\):.*/\\1/g" '], shell=True)
        result = output.decode().split("\n") 
        for iface in result :
            if iface != "" :
                IFACES.append("wifi@" + iface)

        return IFACES   

    
class NetworkConfiguration :
    def __init__(self) :
        self.NETCONF = -1
        self.NETCONF_LIB = ""
        self.NETARRAY = []
        self.NETDHCPCD = False
        self.NFSROOT = False
        
    def conf_net(self, user_input, arg_cmd = "") :

        global TFACES
        
        # Get network interfaces
        IFACES = EN_NETCONF.list_if()
        i = 0
        TFACES = [ *IFACES, *EN_NETCONF.GetArray() ]    
        
        self.NETARRAY = TFACES
        #print(IFACES)
        # list_if send list like "dhcpcd@inet" 
        if arg_cmd in EN_NETCONF.NETWORKMANAGER :
            self.NETCONF = len(IFACES)
            #return
        elif arg_cmd == EN_NETCONF.CONNMAN :
            self.NETCONF = len(IFACES) + 1
        elif arg_cmd == EN_NETCONF.DHCPCD :
            self.NETCONF = len(IFACES) + 2
        elif arg_cmd == EN_NETCONF.NFSROOT:
            #self.NETARRAY =  ["nfsroot"]
            self.NETCONF = len(IFACES) + 3
            self.NETARRAY[self.NETCONF] = "nfsroot"
            self.NFSROOT = True
        else :
            for l_s_net in IFACES :
                if l_s_net == arg_cmd or l_s_net.replace("dhcpcd@", "") == arg_cmd :
                    self.NETCONF = i
                    self.NETDHCPCD = True
                i += 1
                
        if self.NETCONF < 0 :
            MENU_OPT =    {'quit': True, 'default' : len(IFACES) + 2 }
            
            yo = user_input.rid_menu("Choisir", MENU_OPT, *TFACES)
            
            
            if yo > 0 :
                if yo - 1 < len(IFACES) :
                    self.NETDHCPCD = True
                if yo - 1 == len(IFACES) + 3 :
                    #self.NETCONF = len(IFACES) + 3
                    self.NETARRAY[yo - 1] = "nfsroot"
                    self.NFSROOT = True
                self.NETCONF = yo - 1
                    
            else :
                self.NETCONF = -1
    
