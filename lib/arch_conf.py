# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# coding: utf-8
#import subprocess


import time

from multiprocessing.pool import ThreadPool
from lib.user_conf import user_conf
#from lib import exec_cmd
import datetime
import tarfile
from threading import Timer
import os
class arch_conf(user_conf) :
    PACK_MANAGER="pacman"
    PACK_MANAGER_OPTS=""
    PACK_MANAGER_SEARCH="-Ss"
    PACK_MANAGER_INSTALL="-S"  # 
    PACK_MANAGER_UPDATE="-Sy"
    PACK_MANAGER_UPGRADE="-Syu"
    PACK_MANAGER_NOCONFIRM="--noconfirm"
    #PACK_MANAGER_MULTILIB="\n#Multilib configuration\n[multilib]\nInclude = /etc/pacman.d/mirrorlist"
    #PACK_MANAGER_AUR="\n#AUR configuration\n[archlinuxfr]\nServer = http://repo.archlinux.fr/\$arch\nSigLevel = Never"
    
    BASE_PACKAGES = "base base-devel linux"
    user_conf.KERNEL_UPDATE = "mkinitcpio -p linux"

    ARCH_CMD = {
        # Main directories creation
        #"create_root" : "mkdir -m 0755 -p {}/var/{{cache/pacman/pkg,lib/pacman,log}} {}/{{dev,run,etc}}",
        "create_root" : "mkdir -m 0755 -p \"{}/var/cache/pacman/pkg\" \"{}/var/lib/pacman\" \"{}/var/log\" \"{}/dev\" \"{}/run\" \"{}/etc\"", 
        "create_tmp" : "mkdir -m 1777 -p \"{}/tmp\"", 
        "create_proc" : "mkdir -m 0555 -p \"{}/sys\" \"{}/proc\"", 
        
        "kill_gpg-agent" : "pkill gpg-agent", 
        "kill_dirmngr" : "pkill dirmngr", 

        # Timezone/Locales/keymap
        "timezone" : "ln -fs /usr/share/zoneinfo/{} /etc/localtime", # in chroot
        "localeset" : "sed -i s/\\#{}/{}/g /etc/locale.gen", # in chroot
        "localegen" : "locale-gen", # in chroot
        "locale" : "echo LANG=\"{}\" > \"{}/etc/locale.conf\"", 
        "keymap" : "echo KEYMAP={} > \"{}/etc/vconsole.conf\"", 
        "xkeyboard" :  "echo -e \"" + user_conf.KEYBOARD_CONF +"\" > \"{}/etc/X11/xorg.conf.d/00-keyboard.conf\"" ,
        
        # hostname/hosts
        "hostname" : "echo \"{}\" > \"{}/etc/hostname\"", 
        "hosts" : "echo -e \"" + user_conf.HOSTS_ENTRY +"\" > \"{}/etc/hosts\"",
        
        # Passwords/sudo
        "sudoers" : "echo \"{}      ALL=(ALL) ALL\" > \"{}/etc/sudoers.d/{}\"", 
        "lock_account" : "passwd -l {}", # chroot
        
        # Fstab
        "genfstab" : "genfstab -U \"{}\" > \"{}/etc/fstab\"",
        
        # Multilib config in pacman.conf
        "multilib" : "echo -e {} >> {}/etc/pacman.conf",
        # Kernel update
        "mkinitcpio" : "mkinitcpio -p linux", 
        # Mirror conf.
        "mirror_conf" : "sed -i \"s/#Server = {}/Server = {}/g\" \"{}/etc/pacman.d/mirrorlist\"",
        # GPG keys management
        "gpg_init" : "pacman-key --init",
        "gpg_populate" : "pacman-key --populate"
       }
        
    ARCH_CMD_PACMAN = {
        "update" : PACK_MANAGER + " -r {} " + PACK_MANAGER_UPDATE,
        "install_base" : PACK_MANAGER + " -r {} " + PACK_MANAGER_INSTALL + " --needed {}", 
        "gnupg" : "cp -a --no-preserve=ownership /etc/pacman.d/gnupg {}/etc/pacman.d/",
        # 20250209 : Unable to copy mirrorlist file (due to nfs share)
        # "mirrorlist" : "cp -a /etc/pacman.d/mirrorlist {}/etc/pacman.d/",
        "mirrorlist" : "cat /etc/pacman.d/mirrorlist > {}/etc/pacman.d/mirrorlist",
        "install_pack" : PACK_MANAGER + " -r {} " + PACK_MANAGER_INSTALL + " --needed {}",  }

    CMD_NFSROOT = [
        "sed -i \"s/MODULES=(/MODULES=({} /g\" /etc/mkinitcpio.conf",
        "sed s/nfsmount/mount.nfs4/ \"/usr/lib/initcpio/hooks/net\" > \"{}/usr/lib/initcpio/hooks/net_nfs4\"", 
        "echo \"tmpfs   /var/log        tmpfs     nodev,nosuid    0 0\" >> \"{}/etc/fstab\"" ,
        "echo -e \"# For Cups :\n#tmpfs   /var/spool/cups tmpfs     nodev,nosuid    0 0\" >> \"{}/etc/fstab\"",
        "sed -i \"s/BINARIES=(/BINARIES=(\\/usr\\/bin\\/mount.nfs4 /g\" /etc/mkinitcpio.conf" ,
        "sed -i \"s/ fsck//\" /etc/mkinitcpio.conf" ,
        "sed -i \"s/HOOKS=(/HOOKS=(net_nfs4 /g\" /etc/mkinitcpio.conf" ,
        "cp /usr/lib/initcpio/install/net{,_nfs4}", 
        
        "mv /var/log /var/_log",
        "mkdir /var/log", 
        user_conf.KERNEL_UPDATE ]
    
    
    def __init__(self):
        super().__init__() 
        
    def __init__(self, user_input, conf_user, local, graphic, reseau, mirrorlist = True, copykeyring = True):
        super().__init__(local, graphic, reseau, conf_user.GRUB_INSTALL, conf_user.LUKS_PART, conf_user.GRUB_EFI)
        self.PATH_INSTALL = conf_user.PATH_INSTALL 
        self.USER_NAME = conf_user.USER_NAME
        self.USER_PASSWD= conf_user.USER_PASSWD
        self.ROOT_PASSWD= conf_user.ROOT_PASSWD
        self.SUDO = conf_user.SUDO
        self.hostname = conf_user.hostname
        self.DISTRIB = "ArchLinux"
        self.REAL_ROOT = self.PATH_INSTALL
        self.SUDO_SET = conf_user.SUDO_SET
        
        self.GRUB_INSTALL = conf_user.GRUB_INSTALL
        self.RAM_INSTALL = conf_user.RAM_INSTALL
        self.QUIET = conf_user.QUIET
        self.TEST_MODE = conf_user.TEST_MODE
        self.INTERACTIVE = conf_user.INTERACTIVE
        self.PACKAGES += conf_user.PACKAGES
        self.PLYMOUTH = conf_user.PLYMOUTH
        
        if self.INTERACTIVE :
            self.PACK_MANAGER_NOCONFIRM == ""
        
        # Get classes to manage user IO
        self.INPUTS = user_input
        self.COPYMIRRORLIST = mirrorlist
        self.COPYKEYRING = copykeyring
        
        self.HOST_ARCH="x86_64"
        self.NEW_ARCH = ""
        self.MIRROR="http://mirror.f4st.host/archlinux/"
        
        self.PACK_MANAGER="pacman"
        self.PACK_MANAGER_NOCONFIRM = arch_conf.PACK_MANAGER_NOCONFIRM
        self.PACK_MANAGER_SEARCH = self.PACK_MANAGER + " -Ss"
        self.PACK_MANAGER_INSTALL = self.PACK_MANAGER + " " + self.PACK_MANAGER_NOCONFIRM + " -S"  # 
        self.PACK_MANAGER_UPDATE = self.PACK_MANAGER + " " + self.PACK_MANAGER_NOCONFIRM + " -Sy"
        self.PACK_MANAGER_UPGRADE = self.PACK_MANAGER + " " + self.PACK_MANAGER_NOCONFIRM + " -Syu"
        self.PACK_MANAGER_CACHE_DEFAULT = "/var/cache/pacman/pkg"
        self.PACK_MANAGER_CACHE_PKG = conf_user.PACK_MANAGER_CACHE_PKG # self.PACK_MANAGER_CACHE_DEFAULT 

        self.prerequis = "pacman"
        self.PACMAN_CONFIG_FILE = "/etc/pacman.conf"
        
        self.file_zoneinfo = "/usr/share/zoneinfo/zone.tab"
        self.file_kbdmap = "/usr/share/kbd/keymaps"
        self.file_kbdmapext = "map.gz"
        self.file_locale = "/etc/locale.gen"

        now = datetime.datetime.now()
        NAME_ARCHIVE = "arch-install"
        
        # self.ARCH_IMG_URL="http://mirrors.kernel.org/archlinux/iso/latest/archlinux-bootstrap-" +now.strftime("%Y.%m.01")+ "-" + self.HOST_ARCH + ".tar.gz"
        self.ARCH_IMG_URL="http://mirrors.kernel.org/archlinux/iso/latest/archlinux-bootstrap-" + self.HOST_ARCH + ".tar.zst"
        print(self.ARCH_IMG_URL)
        # self.ARCH_IMG_URL="http://calis-asso.org/Event1.tar.gz"
        self.ARCH_IMG_FILE = "/tmp/" + NAME_ARCHIVE + ".tar.gz"
        self.DOWNLOAD_OK = True
        self.NO_PACMAN = False
        self.ROOT_BOOTSTRAP = self.DIR_WORK + "/root.x86_64"
        self.ROOT_DIR_BOOTSTRAP="/install.arch"
        self.LOG_EXE="/tmp/anarchi.log"
        self.FILE_COMMANDS="/tmp/anarchi_command"
        self.SYSTEMD_ENABLE = self.GetPackages("systemd_enable")
        
        self.REAL_ROOT = self.PATH_INSTALL
        
        user_input.msg_n("Checking {}", "32", "32", "environnement")
        if not self.check_command(self.prerequis) :
            if not os.path.exists(self.ROOT_BOOTSTRAP) :
                if not os.path.exists(self.ARCH_IMG_FILE) :
                    if self.INPUTS.rid_continue("Télécharger {} ?", "Arch") :
                        #self.NO_PACMAN = True
                        #print("Downloading ... " + self.ARCH_IMG_URL)
                        ThreadPool(1).imap_unordered(self.download_img, [(self.ARCH_IMG_FILE ,self.ARCH_IMG_URL)])
                        self.DOWNLOAD_OK = False
                        #start = time()
                        
                    else :
                        self.INPUTS.die("Pas de {}, pas de {} !", "pacman", "chocolat")
                else :
                    self.WaitForExtract(True)
            self.NO_PACMAN = True
            self.locale.file_zoneinfo = self.ROOT_BOOTSTRAP + "/usr/share/zoneinfo/zone.tab"
            self.locale.file_kbdmap = self.ROOT_BOOTSTRAP + "/usr/share/kbd/keymaps"

            self.locale.file_locale = self.ROOT_BOOTSTRAP + "/etc/locale.gen"
        else :
            if not self.exist_install(self.INPUTS, "genfstab", "arch-install-scripts") :
                self.INPUTS.die("Pas de {}, pas de {} !", "genfstab", "chocolat")
          
        self.AddPackages()
        self.INPUTS.LOADING = False

    def extract_tar(self) :
        try :
            tar = tarfile.open(self.ARCH_IMG_FILE, "r:gz")
            tar.extractall(path=self.DIR_WORK)
            tar.close()
            return True
        except Exception as e:
            print()
            raise Exception("Extract failed ! \n{}".format(e))
            # return False
            
    def WaitForExtract(self, run_loading = True) :
        try :
            if not os.path.exists(self.ROOT_BOOTSTRAP) :
                self.INPUTS.msg_nn("Please wait while {} archlinux boostrap image...", "extracting")
                if run_loading :
                    self.INPUTS.loading()
                #ThreadPool(2).imap_unordered(self.INPUTS.loading, [( user_input.msg_nn("Extracting {}...", self.ARCH_IMG_FILE) ) ])
                self.INPUTS.LOADING = True
                self.extract_tar()
                self.INPUTS.LOADING = False       
                print("\bdone")
        except Exception as e:
            print(e)
            if os.path.exists(self.ROOT_BOOTSTRAP) :
                os.rmdir(self.ROOT_BOOTSTRAP)

    def SetLang(self) :
        try :
            # Affecte le
            self.running.add_chroot(arch_conf.ARCH_CMD["localeset"].format(self.GetLocale(), self.GetLocale()), self.PATH_INSTALL)
            # Modification de la locale du systeme dans le fichier /etc/locale.conf
            self.running.add(arch_conf.ARCH_CMD["locale"].format(self.GetLocale(), self.PATH_INSTALL))
            # Generation de la locale avec locale-gen
            self.running.add_chroot(arch_conf.ARCH_CMD["localegen"], self.PATH_INSTALL)
            # Création du lien vers la bonne timezone dans : /usr/share/zoneinfo/
            self.running.add_chroot(arch_conf.ARCH_CMD["timezone"].format(self.GetTimeZone()), self.PATH_INSTALL)
            # Modification du codage clavier
            self.running.add(arch_conf.ARCH_CMD["keymap"].format(self.GetKeymap(), self.PATH_INSTALL))
        except Exception as ex :
            self.INPUTS.error("Error come from {}"".\n" + ex, self.__name__)

    def next_cmd(self) :
        return super().next_cmd()
    
    def install(self) :
        # # Installation en RAM : On créer un nouveau dossier qui devient la nouvelle racine,
        # # on créer un nouveau dossier en ram pour le monter dans le
        # if self.RAM_INSTALL :
        #     #self.REAL_ROOT = self.PATH_INSTALL
        #     self.PATH_INSTALL = self.PATH_INSTALL + "/tmp_install"
        #     #os.mkdir(self.PATH_INSTALL)
        #     #os.mkdir()
        #     if chroot.run_command.run("mkdir -p /tmp/tmp_install " + self.PATH_INSTALL) == 0 :
        #         if chroot.run_command.run("mount -o bind /tmp/tmp_install " + self.PATH_INSTALL) == 0 :
        #             self.INPUTS.message(self.INPUTS.msg_nn.__name__, "Install RAM {}", "32", "32", "OK")
        #         else :
        #             if self.INPUTS.rid_continue("{} create RAM {}", "Error", "Continue in normal mode ?" ) :
        #                 self.PATH_INSTALL = self.REAL_ROOT
        #             else :
        #                 self.INPUTS.die("Ram install failed !")
                        
        if self.NO_PACMAN :
            self.running = user_conf.chroot.chroot_init(self.ROOT_BOOTSTRAP, self.TEST_MODE, self.QUIET, True)
            #if self.QUIET :
            #self.PATH_INSTALL = self.ROOT_BOOTSTRAP
            #print(chroot.run_command.run("mkdir -p " + self.ROOT_BOOTSTRAP + self.ROOT_DIR_BOOTSTRAP))
            if not user_conf.chroot.run_command.run("mkdir -p " + self.ROOT_BOOTSTRAP + self.ROOT_DIR_BOOTSTRAP) == 0 :
                self.INPUTS.die("Unable to create arch root to launch pacman !")
                
            if not self.running.add_mount(self.ROOT_BOOTSTRAP + self.ROOT_DIR_BOOTSTRAP, self.PATH_INSTALL, "none", "bind") :
                self.INPUTS.die("Unable to mount !")
                
            if not self.running.add_mount(self.ROOT_BOOTSTRAP + "/etc/resolv.conf", "/etc/resolv.conf", "none", "bind") :
                self.INPUTS.die("Unable to mount resolv!")
                
            self.PATH_INSTALL = self.ROOT_BOOTSTRAP + self.ROOT_DIR_BOOTSTRAP
        else :
            self.running = user_conf.chroot.chroot_init(self.PATH_INSTALL, self.TEST_MODE, self.QUIET, True)
            
        if not self.SUDO :
            self.OPTPASSWD += " -G wheel"
        
        if not self.anarchi_create_root() :
            self.INPUTS.die("Error create root !")
        
        if not self.running.Init() :
            self.INPUTS.die("Error chroot config !")

        # Installation
        super().install()
        
        # if self.RAM_INSTALL :
        #     #self.REAL_ROOT = self.PATH_INSTALL
        #     chroot.chroot_init.umount_disk()
        #     if chroot.run_command.run("umount \"" + self.PATH_INSTALL + "\"") == 0 :
        #         chroot.run_command.run("sleep .5 && rmdir \"" + self.PATH_INSTALL + "\"")
        #         self.PATH_INSTALL = self.REAL_ROOT
        #         self.INPUTS.message(self.INPUTS.msg_nn.__name__, "Deplacement des fichiers de {} vers {}", "/tmp/tmp_install", self.PATH_INSTALL)
        #         if chroot.run_command.run("mv /tmp/tmp_install/* \"" + self.PATH_INSTALL + "\"" ) == 0 :
        #             #print(user_input.msg_nn("{}", "OK !"))
        #             self.running = chroot.chroot_init(self.PATH_INSTALL, self.TEST_MODE, self.QUIET)
        #             if not self.running.Init() :
        #                 self.INPUTS.LOADING = False
        #                 self.INPUTS.die("Error chroot config !")
        #             self.running.run_chroot("mkinitcpio -p linux")

    def anarchi_create_root(self) :
        """ Create initial hierarchy """
        RACINE = self.PATH_INSTALL
        
        self.running.add(str(arch_conf.ARCH_CMD["create_root"].format(RACINE, RACINE,RACINE, RACINE,RACINE, RACINE)))
        self.running.add(arch_conf.ARCH_CMD["create_tmp"].format(RACINE))
        self.running.add(arch_conf.ARCH_CMD["create_proc"].format(RACINE, RACINE))        

        return self.running.run_all_once( self.anarchi_create_root.__name__ ) == 0
        
    def anarchi_base(self) :
        """ Install base system """
        #BASE_PACKAGES = "base base-devel linux"
        RACINE = self.PATH_INSTALL
        CHROOT_PARAM = ""
        cmd_update = arch_conf.ARCH_CMD_PACMAN["update"]
        cmd_install = arch_conf.ARCH_CMD_PACMAN["install_base"]
        cmd_copykeyring = arch_conf.ARCH_CMD_PACMAN["gnupg"]
        cmd_mirrorlist = arch_conf.ARCH_CMD_PACMAN["mirrorlist"]

        if self.NO_PACMAN :
            RACINE = self.ROOT_DIR_BOOTSTRAP

            if self.PACK_MANAGER_CACHE_PKG != "" :
                self.running.add_mount(self.ROOT_BOOTSTRAP + self.PACK_MANAGER_CACHE_DEFAULT, self.PACK_MANAGER_CACHE_PKG, "none", "bind") 
                
            if not self.running.Init(self.PATH_INSTALL) :
                self.INPUTS.die("Error chroot config !")
            if not self.QUIET or self.TEST_MODE :
                self.INPUTS.msg_nn2("Init pacman-key on {}", self.ROOT_BOOTSTRAP)
                print()
            self.anarchi_gpg_init_root(self.ROOT_BOOTSTRAP, "anarchi_gpg_init_bootstrap")
            self.running.add_chroot(cmd_update.format(RACINE), self.ROOT_BOOTSTRAP)
            self.running.add_chroot(cmd_install.format(RACINE, self.PACK_MANAGER_OPTS + " " + self.PACK_MANAGER_NOCONFIRM  + " " + arch_conf.BASE_PACKAGES), self.ROOT_BOOTSTRAP)
            
            if self.COPYKEYRING :
                if os.path.exists(self.ROOT_BOOTSTRAP + "/etc/pacman.d/gnupg") and os.path.isdir(self.ROOT_BOOTSTRAP + "/etc/pacman.d/gnupg") and not os.path.exists(RACINE + "/etc/pacman.d/gnupg") :
                    self.running.add_chroot(cmd_copykeyring.format(RACINE))
                
            if self.COPYMIRRORLIST :
                self.running.add_chroot(cmd_mirrorlist.format(RACINE))
        else :
            if self.PACK_MANAGER_CACHE_PKG != "" :
                self.PACK_MANAGER_OPTS = "--cachedir " + self.PACK_MANAGER_CACHE_PKG 

            if self.COPYKEYRING :
                self.running.add(cmd_copykeyring.format(RACINE))
            else :
            #     # Keys copy are disabled, we're gonna generate ourselves
            #     # self.running.add_chroot(arch_conf.ARCH_CMD["gpg_init"], RACINE)
            #     # self.running.add_chroot(arch_conf.ARCH_CMD["gpg_populate"], RACINE)
            #     #
                self.running.add("pacman-key --gpgdir \"{}/etc/pacman.d/gnupg\" --init".format(RACINE))

            self.running.add(cmd_update.format(RACINE))
            self.running.add(cmd_install.format(RACINE, self.PACK_MANAGER_OPTS + " " + self.PACK_MANAGER_NOCONFIRM  + " " + arch_conf.BASE_PACKAGES))
                
            # print(self.COPYMIRRORLIST)
            if self.COPYMIRRORLIST :
                self.running.add(cmd_mirrorlist.format(RACINE))
                
        return self.running.run_all_once(self.anarchi_base.__name__) == 0

# BEGIN GPG config
    #def anarchi_gpg_init_bootstrap(self) :
        #""" #Init GPG database on boostrap directory """
        #self.anarchi_gpg_init(self.ROOT_BOOTSTRAP)
        #return self.running.run_all_once( self.anarchi_gpg_init_bootstrap.__name__) == 0
  
    def anarchi_gpg_init_root(self, racine, func_name) :
        """ Init GPG database on chrooted install """
        self.anarchi_gpg_init(racine)
        return self.running.run_all_once( func_name) == 0      

    def anarchi_gpg_init(self, racine) :
        """ Init GPG database on directory """
        mirror = self.MIRROR.replace("/", "\\/")
        self.running.add(arch_conf.ARCH_CMD["mirror_conf"].format(mirror, mirror, racine))
        self.running.add_chroot(arch_conf.ARCH_CMD["gpg_init"], racine)
        self.running.add_chroot(arch_conf.ARCH_CMD["gpg_populate"], racine)
        self.running.add_chroot(arch_conf.ARCH_CMD["kill_gpg-agent"], racine)
        self.running.add_chroot(arch_conf.ARCH_CMD["kill_dirmngr"], racine)
        self.running.add_chroot(self.PACK_MANAGER_UPDATE, racine)
# END GPG Conf

    def anarchi_conf(self) :
        res_cmd = 0
        try :
            if not os.path.exists(user_conf.chroot.chroot_init.DIR_DONE) :
                os.mkdir(user_conf.chroot.chroot_init.DIR_DONE)
            if not os.path.exists(user_conf.chroot.chroot_init.DIR_DONE + "/" + self.anarchi_conf.__name__) :
                self.running.add(arch_conf.ARCH_CMD["hostname"].format(self.hostname, self.PATH_INSTALL)) 
                self.running.add(arch_conf.ARCH_CMD["hosts"].format(self.hostname, self.hostname, self.PATH_INSTALL))
                
                self.SetLang()                
                
                if self.GetNetworkConfig() != "nfsroot" :
                    if self.NO_PACMAN :
                        #CMD_CREATE = arch_conf.ARCH_CMD["genfstab"].format(self.ROOT_DIR_BOOTSTRAP, self.PATH_INSTALL)
                        self.running.add_chroot(arch_conf.ARCH_CMD["genfstab"].format(self.ROOT_DIR_BOOTSTRAP, self.PATH_INSTALL), self.ROOT_BOOTSTRAP)
                        #print(CMD_CREATE + " " + self.ge)
                        #self.running.run_chroot("bash", self.ROOT_BOOTSTRAP)
                    else :                        
                        #CMD_CREATE = arch_conf.ARCH_CMD["genfstab"].format(self.PATH_INSTALL, self.PATH_INSTALL)
                        self.running.add(arch_conf.ARCH_CMD["genfstab"].format(self.PATH_INSTALL, self.PATH_INSTALL))
                #print("6" + self.running.LIST)
                return self.running.run_all_once( self.anarchi_conf.__name__ ) == 0
        except Exception as e :
            print("Error come from {}"".\n" + e)
        finally :
            return res_cmd == 0
    
    
    def anarchi_packages(self) :
        RACINE = self.PATH_INSTALL
        
        if self.NO_PACMAN :
            RACINE = self.ROOT_DIR_BOOTSTRAP
            self.running.add_chroot((arch_conf.ARCH_CMD_PACMAN["install_pack"].format(RACINE, self.PACK_MANAGER_OPTS + " " +self.PACK_MANAGER_NOCONFIRM + " " + self.PACKAGES_AUTO.strip()  + " " + self.PACKAGES.strip())), self.ROOT_BOOTSTRAP)

        else :
            self.running.add(arch_conf.ARCH_CMD_PACMAN["install_pack"].format(RACINE, self.PACK_MANAGER_OPTS + " " +self.PACK_MANAGER_NOCONFIRM + " " + self.PACKAGES_AUTO.strip()  + " " + self.PACKAGES.strip()))
    
        res_pak = self.running.run_all_once(self.anarchi_packages.__name__)

        if res_pak > 0 :
            return res_pak == 0
        
        # Creation du fichier /etc/X11/xorg.conf.d/00-keyboard.conf
        if self.graphic.DESKTOP_ENV_IDX > -2 :
            CMD_CREATE = "mkdir -p " + self.PATH_INSTALL + "/etc/X11/xorg.conf.d/\n" + arch_conf.ARCH_CMD["xkeyboard"].format(self.GetXKbdmap().split("_")[0], self.PATH_INSTALL)
            
            res_cmd = user_conf.chroot.run_command.run(user_conf.USER_CMD_CHROOT.format("bash", CMD_CREATE))
        return res_cmd == 0

    def anarchi_nfs_root(self) :
        arch_conf.CMD_NFSROOT[0] = arch_conf.CMD_NFSROOT[0].format(self.LIST_MODULES)
        arch_conf.CMD_NFSROOT[1] = arch_conf.CMD_NFSROOT[1].format(self.PATH_INSTALL)
        arch_conf.CMD_NFSROOT[2] = arch_conf.CMD_NFSROOT[2].format(self.PATH_INSTALL)
        arch_conf.CMD_NFSROOT[3] = arch_conf.CMD_NFSROOT[3].format(self.PATH_INSTALL)

        return user_conf.anarchi_nfs_root(self, arch_conf.CMD_NFSROOT)
  
    def anarchi_post_install(self) :
        self.anarchi_gpg_init_root(self.PATH_INSTALL, "anarchi_gpg_init_root")
        return True

    def anarchi_luks(self) :
        # GRUB config
        super().anarchi_luks()
        # mkinitcpio config
        # user_conf.CMD_LUKS_MKINITCPIO = user_conf.CMD_LUKS_MKINITCPIO.format(user_conf.HOOKS_LUKS_MKINITCPIO[0], user_conf.HOOKS_LUKS_MKINITCPIO[1])
    # "sed -i \"s/\(HOOKS=(.*kms\)\(.*block\) /\\1 " + HOOKS_LUKS_MKINITCPIO[0] + "\\2 " + HOOKS_LUKS_MKINITCPIO[1] + " /g\" {}/etc/mkinitcpio.conf"
        cmd_luks = user_conf.CMD_LUKS_MKINITCPIO.format(user_conf.HOOKS_LUKS_MKINITCPIO[0], user_conf.HOOKS_LUKS_MKINITCPIO[1], self.PATH_INSTALL)
        # print(cmd_luks)
        self.running.add(cmd_luks)
        # Kernel update
        self.running.add_chroot(user_conf.KERNEL_UPDATE)

        return self.running.run_all_once( self.anarchi_luks.__name__ ) == 0
