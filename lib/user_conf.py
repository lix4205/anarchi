# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# coding: utf-8
import subprocess
import time
#from multiprocessing.pool import ThreadPool
from lib import system_conf
import os
import json
from .pythcmd import chroot

class user_conf :
    chroot = chroot
    #ARCHS = ("x86_64", "i386")
    DEBIANVERSIONS = ["jessie", "stretch", "buster", "bullseye", "bookworm", "sid"]

    HOSTS_ENTRY = "# /etc/hosts: static lookup table for host names\n# See hosts(5) for details.\n#<ip-address>\t<hostname.domain.org>\t<hostname>\n127.0.0.1\tlocalhost.localdomain\tlocalhost\t{}\n::1\tlocalhost.localdomain\tlocalhost\t{}"
    KEYBOARD_CONF = "Section \"InputClass\"\n\tIdentifier \"system-keyboard\"\n\tMatchIsKeyboard \"on\"\n\tOption \"XkbLayout\" \"{}\"\nEndSection"
    NOCONFIRM = "--noconfirm"
    FSTAB_BEGIN = "# Static information about the filesystems.\n# See fstab(5) for details.\n\n# <file system> <dir> <type> <options> <dump> <pass>"
    GRUB_BIN="/usr/bin/grub-mkconfig"
    
    GRUB_CONFIG = "sed -i \"0,/.*GRUB_CMDLINE_LINUX=\\\"/s//GRUB_CMDLINE_LINUX=\\\"{}/g\" \"{}/etc/default/grub\""
    GRUB_ENTRIES = "\nmenuentry \\\"System shutdown\\\" {{\n\techo \"System shutting down...\"\n\thalt\n}}"
    GRUB_ENTRIES += "\n\nmenuentry \\\"System restart\\\" {{\n\techo \"System rebooting...\"\n\treboot\n}}"
    GRUB_CMD = {
        "install" : "/usr/sbin/grub-install --recheck {}", 
        "install_efi" : "/usr/sbin/grub-install --target=x86_64-efi --bootloader-id=grub_{} --recheck --efi-directory=/boot/",
        #"update_ArchLinux" : "grub-mkconfig -o /boot/grub/grub.cfg", 
        "update_ArchLinux" : "grub-mkconfig -o /boot/grub/grub.cfg", ### par defaut
        "update_gentoo" : "grub-mkconfig -o /boot/grub/grub.cfg", ### par defaut
        "update_debian" : "/usr/sbin/update-grub", 
        "power" : "echo -e \"" + GRUB_ENTRIES + "\" >> {}/etc/grub.d/40_custom",
        # "luks_config" : "sed -i \"s/GRUB_CMDLINE_LINUX=.*/GRUB_CMDLINE_LINUX={}/g\" \"{}/etc/default/grub\"",
        # "plymouth_config" : "sed -i \"s/GRUB_CMDLINE_LINUX=\\\"/GRUB_CMDLINE_LINUX=\\\"{}/g\" \"{}/etc/default/grub\"",

        # GRUB_CMDLINE_LINUX="crypt_root=UUID=6a7a642a-3262-4f87-9540-bcd53969343b root=/dev/mapper/root_{}"
        }

    # HOOKS=(base udev autodetect microcode modconf kms keyboard keymap consolefont block encrypt filesystems fsck)
# sed "s/\(HOOKS=(.*kms\)\(.*block\) /\1 keyboard keymap consolefont\2 encrypt /g"

    HOOKS_LUKS_MKINITCPIO = [ "keyboard keymap consolefont", "encrypt" ]
    CMD_LUKS_MKINITCPIO = "sed -i \"s/\\(HOOKS=(.*kms\\)\\(.*block\\) /\\1 {}\\2 {} /g\" {}/etc/mkinitcpio.conf"
    USER_CMD = {
        # Add user
        "useradd" : "useradd {} {}",
        #"useradd_arch" : "useradd -m -g users -G wheel -s /bin/bash {}", 
        # Other options
        # Sudo or not sudo
        "sudoers" : "mkdir -p {}/etc/sudoers.d && echo \"{}      ALL=(ALL) ALL\" > \"{}/etc/sudoers.d/{}\"",
        # "sudoers" : "echo \"{}      ALL=(ALL) ALL\" > \"{}/etc/sudoers.d/{}\"",
        # Lock root password in case of sudoer
        "lock_account" : "passwd -l {}", 
        
        }
    # For password, we have to copy an external script on the new root, and execute it
    # because chroot can't execute whole command when it use pipe '|' on new root ...
    #USER_PASSWD = "passwd {} < {}"
    USER_CMD_PASSWD = "echo {}:\"{}\" | chpasswd\n"
    # Command using EOF to insert commands with pipes '|'
    USER_CMD_CHROOT = "{} <<EOF\n{}\nEOF"
    
    SYSTEMD_SERVICE = "systemctl enable {}"
    SYSTEM_CONF = system_conf.DistConf
    CUSTOM_SCRIPT_DIR = "/tmp/custom.d"
    CUSTOM_SCRIPT = os.path.join(CUSTOM_SCRIPT_DIR, "custom")

    def __init__(self, locale, graphic, reseau, grub_disk = "", luks = "", uefi = False):
        self.PATH_INSTALL = ""
        self.USERS = {}
        #self.USERS_PASSWD = ()
        self.USER_NAME = ""
        self.USER_PASSWD = ""
        self.ROOT_PASSWD= ""
        self.SUDO = False
        self.SUDO_SET = False
        self.hostname = ""
        self.DISTRIB = ""
        self.DEBIAN_VERS = ""
        self.PACK_MANAGER = ""
        self.PACK_MANAGER_SEARCH = ""
        self.PACK_MANAGER_INSTALL = ""
        self.PACK_MANAGER_UPDATE = ""
        self.PACK_MANAGER_UPGRADE = ""
        self.PACK_MANAGER_CACHE_PKG=""
        self.PACK_MANAGER_OPTS = ""
        self.PACKAGES = ""
        self.PACKAGES_AUTO = ""
        self.locale = locale
        self.graphic = graphic
        self.reseau = reseau
        self.RAM_INSTALL = False
        self.GRUB_INSTALL = grub_disk

        self.GRUB_EFI = uefi
        self.LUKS_PART = luks
        self.UUID_ROOT = ""
        self.PLYMOUTH = False

        # print("conf_user.PACKAGES_AUTO" + str(self.GRUB_INSTALL))
        #print("## :" + str(self.reseau.GetNetworkConfig()) + " | :" + str(reseau.GetNetworkConfig()))
        #conf_user.reseau.GetNetworkConfig()
        self.HOST_ARCH = ""
        self.NEW_ARCH = ""
        self.QUIET = False
        self.TEST_MODE = False
        self.prerequis = ""
        self.INTERACTIVE = False
        #self.prerequis = ""
        #self.DOWNLOAD_OK = True
        
        self.DIR_WORK = "/tmp"
        self.DIR_DONE = "/tmp/done"
        self.LOG_EXE="/tmp/anarchi.log"
        self.FILE_COMMANDS="/tmp/anarchi_command"
        self.SYSTEMD_ENABLE = ""
        self.LIST_MODULES = system_conf.DistConf.MOD_NFSROOT
        self.NFSROOT = False
        self.EXPORT_BASH = "export NAME_USER={} GRUB_DISK={} NFSROOT={} DE={} DM={} X11_KEYMAP={} DEBIAN_INSTALL={} CACHE_PKG={}"
            #self.PACKAGES += " grub os-prober"
        self.LIST_CMD = []
        self.running = None
        self.LIST_ANARCHI = {}
        
        self.OVERRIDE_PATH = ""
        self.OPTPASSWD ="-m -g users -s /bin/bash"
        self.CUPS_PACK = ""
        self.BLUEZ_PACK = ""

        # sys.exit(0)

# BEGIN Misc functions
    def AddPackages(self) :
        self.PACKAGES_AUTO += self.GetPackages("pack_common")
        if self.GRUB_INSTALL != "" or self.GRUB_EFI :
            self.PACKAGES += self.GetPackages("pack_grub")
            if self.GRUB_EFI :
                self.PACKAGES += self.GetPackages("pack_grub_efi")
            else :
                self.PACKAGES += self.GetPackages("pack_grub")
        
        # print("conf_user.PACKAGES_AUTO" + str(self.GRUB_INSTALL))
    
        if self.graphic.VID_DRIVERS_IDX > 0 :
            self.PACKAGES_AUTO += self.GetPackages("pack_nav")
            self.PACKAGES_AUTO += self.GetPackages("pack_desktop")

        if self.PLYMOUTH :
            self.PACKAGES += self.GetPackages("pack_plymouth")
        

    def GetPackages(self, *package) :
        return user_conf.GetPackagesFromKeys(self.DISTRIB, *package)
        
    def SearchPackage(self, packages, chroot = False, override_path = False) :
        ERR_PKG = ""
        IN_CHROOT = ""

        if self.TEST_MODE :
            return ""
        
        if chroot :
            IN_CHROOT = "chroot \"" + self.PATH_INSTALL + "\" "
        
        cmd_search = IN_CHROOT + "{} {} | grep -q \"{}\" >> /dev/null"
        if override_path :
            cmd_search = USER_CONf.USER_CMD_CHROOT.format("bash", self.EXPORT_BASH + "\n" + cmd_search)
        
        str2search = "{}"
        if self.DISTRIB == "ArchLinux" :
            str2search = "^.*/{} "
        elif self.DISTRIB == "debian" :
            str2search = "^{}/"
        elif self.DISTRIB == "gentoo" :
            str2search = "^*  .*/"

        self.PACKAGES = ""
        for pkg in packages.strip().split(" ") :
            #print("pk" + pkg) 
            if pkg.strip() != "" :
                if not self.running.check_call(cmd_search.format(self.PACK_MANAGER_SEARCH, pkg.strip(), str2search.format(pkg.strip()))) == 0 :
                    ERR_PKG += " " + pkg
                else :
                    self.PACKAGES += " " + pkg
                
        if ERR_PKG.strip() != "" :
            return ERR_PKG.strip()
        
        return ""

    def GetPackagesFromKeys(*keys) :
        l_s_val = system_conf.DistConf.DEFAULT_CONF[keys[0]]
        l_s_value = ""
        
        for l_s_key in keys[1:] :
            l_s_val = l_s_val[l_s_key]
        
        if not type(l_s_val) == str :
            for l_s_txt in l_s_val :
                l_s_value += " " + l_s_txt
        else :
            l_s_value += " " + l_s_val
        
        return l_s_value
    
    def SetAdditionnalPackage(self, addbluetooth, addhplip, addcups, addOffice, addMail ) :
        if addbluetooth :
            self.PACKAGES_AUTO += self.GetPackages("pack_bluez")
        if addhplip :
            self.PACKAGES_AUTO += self.GetPackages("pack_hplip")
            addcups = True
        if addcups :
            self.PACKAGES_AUTO += self.GetPackages("pack_cups")
        if addOffice :
            self.PACKAGES_AUTO += self.GetPackages("pack_office")
        if addMail :
            self.PACKAGES_AUTO += self.GetPackages("pack_mail")
    
    def download_img(self, url) :
        try:
            import requests
            path, url = url
            r = requests.get(url, stream = True)
            #print(str(r))
            self.DOWNLOAD_OK = False
            with open(path + ".part", 'wb') as f:
                for ch in r:
                    f.write(ch)
            self.DOWNLOAD_OK = True
            os.rename(path + ".part", path)
        except Exception as ex:
            self.DOWNLOAD_OK = True
           
            # print("==> ERROR : Unable to retrieve \n  -> ")
            # return 1
            raise Exception("Unable to retrieve {}\n{}".format(self.ARCH_IMG_URL, ex))
       
    def WaitForDownload(self, interrupt = False, *msg_replace) :
        try :
            time.sleep(0.5)
            if not self.DOWNLOAD_OK :
                self.INPUTS.msg_nn(*msg_replace)
                #ThreadPool(2).imap_unordered(user_input.loading, [( user_input.msg_nn(*msg_replace) ) ])
                #user_input.loading()
                #timeout = 1
                #user_input.LOADING = True
                self.INPUTS.loading()
                while not self.DOWNLOAD_OK :
                    #t = Timer(timeout, user_input.msg_nn, [*msg_replace])
                    #t.start()
                    ##prompt = "You have %d seconds to choose the correct answer..." % timeout
                    #answer = user_input.ask1char("")
                    time.sleep(1)
                    #if self.DOWNLOAD_OK or ( interrupt and answer != "") :
                        #break
                    #t.cancel()
                    
                #print("Fin attnte")
            #print("\bdddd")
                self.INPUTS.LOADING = False
                print("\bdone")
            self.WaitForExtract(False)
            return True
        except :
            print()
            self.INPUTS.die("Error occured while retrieving Archlinux bootstrap")
            return False

    def WaitForExtract(self, run_loading = True) :
        return 1
    
    def check_command(self, cmd) :
        rc = subprocess.call(['which', cmd], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        if (rc == 0) :
            return True
        else:
            return False

    def find_pack_manager(self) :
        # PACK_MANAGER = [ "yum" , "pacman", "apt", "apt-get" ]

        for pacman in system_conf.DistConf.PACK_MANAGER :
            if self.check_command(pacman) :
                return pacman

        user_input.error("Unable to find known package manager to install !")
        
    def exist_install(self, user_input, cmd, package) :
        """ Search for a package, then install it from repo if not installed """
        if (self.check_command(cmd)) :
            user_input.msg_n2('{} installed', "32", "32", cmd )
        else :
            #if self.check_command(self.PACK_MANAGER) :
            if package == "" :
                package = cmd
            # print(self.TEST_MODE)
            if self.TEST_MODE :
                return True
            # self.find_pack_manager(user_input)
            #if self.check_command(self.PACK_MANAGER) :
            if user_input.rid_continue("Execute : {}", self.PACK_MANAGER_UPDATE) :
                if os.geteuid() == 0  :
                    rc = subprocess.call([*self.PACK_MANAGER_UPDATE.split(" ")])
                    if rc > 0 :
                        return False
                else :
                    user_input.error("Only {} can continue : ", "root")
                    return False
            if user_input.rid_continue("Execute : {} {}", self.PACK_MANAGER_INSTALL, package) :
                if os.geteuid() == 0  :
                    rc = subprocess.call([*self.PACK_MANAGER_INSTALL.split(" "), package])
                    if rc > 0 :
                        return False
                else :
                    user_input.error("Only {} can continue : ", "root")
                    return False
            else :
                return False

        return True

# END Misc

# BEGIN Misc config (username/hostname/arch)
    def SetUser(self, message, name_user = "") :
        if name_user != "" :
            if name_user != "root" :
                self.USER_NAME = name_user 
            else :
                message.error("Unable to set username as root !")
                name_user = ""
                
        if not os.path.exists("/tmp/done/anarchi_passwd") :
            while name_user == "" :
                name_user = message.ask("Veuillez entrer un login pour l'utilisateur :")
                if name_user != "root" :
                    self.USER_NAME = name_user 
                else :
                    message.error("Unable to set username as {} !", "root")
                    name_user = ""
                
            # TODO : Confirmation password + hide text...
            if not self.SUDO_SET :
                if message.rid_continue("Use sudo ?") :
                    self.SUDO = True
                self.SUDO_SET = True
            
            if not self.SUDO :
                #self.USERS.add("root")
                while self.ROOT_PASSWD == "" :
                    self.ROOT_PASSWD = message.ask4pass("Veuillez entrer un password pour l'utilisateur : {}", "root")
                    self.USERS["root"] = self.ROOT_PASSWD
                    
            while self.USER_PASSWD == "" : 
                self.USER_PASSWD = message.ask4pass("Veuillez entrer un password pour l'utilisateur : {}", self.USER_NAME)
            
            #self.USERS.add(name_user, 
            self.USERS[name_user] = self.USER_PASSWD
            #self.USERS_PASSWD.append(self.USER_PASSWD, self.ROOT_PASSWD)
         
    def SetHostname(self, message, hostname = "") :       
        if hostname != "" :
            self.hostname = hostname 
                
        while hostname == "" :
            hostname = message.ask("Veuillez entrer un nom de machine :")
            if hostname != "" :
                self.hostname = hostname 
  
    def SetArch(self, user_input, arch = '') :
        """ Set architecture """
        output = subprocess.check_output(["uname -m" ], shell=True)
        arch_cur = output.decode().replace("\n", "")
        archs = list(system_conf.DistConf.ARCHS.keys())

        # print(arch_cur + " == " + archs[1] + " && " +  arch + " != " + archs[1])
        if self.DISTRIB.startswith("debian") :
            if arch in archs and (arch_cur in system_conf.DistConf.ARCHS[arch]):
                self.NEW_ARCH = arch
            else :
                self.NEW_ARCH = archs[user_input.rid_menu("Veuillez choisir une architecture ", { "quit" : True } ,*archs) -1]
            self.INPUTS.msg_n("Architecture {} choisie", "32", "32", self.NEW_ARCH)
        
        elif arch_cur != "x86_64" :
            user_input.die("Arch x64")
        else :
            self.NEW_ARCH = archs[0]

# END Misc config (username/hostname/arch)  
    
    def next_cmd(self) :
        str2return = ""
        if self.USER_NAME != "" :
            str2return += " --username " + self.USER_NAME 
        if self.hostname != "" :
            str2return += " --hostname " + self.hostname 
        if self.locale.LOCALE_IDX > -1 :
            str2return += " --locale " + self.GetLocale() 
        if self.locale.ZONE > -1 :
            str2return += " --zone " + self.GetTimeZone() 
        if self.locale.KBDMAP_IDX > -1 :
            str2return += " --keymap " + self.GetKeymap() 
        if self.locale.XKBDMAP_IDX > -1 :
            str2return += " --xkbmap " + self.GetXKbdmap() 
        if self.graphic.VID_DRIVERS_IDX > -1 :
            str2return += " --graphic " + self.GetVidDriver() 
        if self.graphic.DESKTOP_ENV_IDX > -1 :
            str2return += " --environment " + self.GetEnvironement() 
        if self.graphic.DISPLAYMANAGER_IDX > -1 :
            str2return += " --displaymanager " + self.GetDisplayManager() 
        if self.PACK_MANAGER_OPTS != "" :
            str2return += " " + self.PACK_MANAGER_OPTS 
        #if self.DISTRIB.startswith("debian") :
            ##str2return += " --debian " 
            #str2return += " --" + self.DEBIAN_VERS
        if self.reseau.NETCONF > -1 :
            str2return += " --network " + self.reseau.NETARRAY[self.reseau.NETCONF]
        if self.SUDO_SET :
            if self.SUDO :
                str2return += " --sudo"
            else :
                str2return += " --no-sudo"
        if self.QUIET :
            str2return += " --quiet"
        if self.GRUB_INSTALL != "" :
            str2return += " --loader " + self.GRUB_INSTALL 
        if self.PACK_MANAGER_CACHE_PKG != "" :
            str2return += " --cachedir \"" + self.PACK_MANAGER_CACHE_PKG + "\""
        if self.TEST_MODE :
            str2return += " --test"
        if self.INTERACTIVE :
            str2return += " --interactive"
        if self.GRUB_EFI :
            str2return += " --efi"
        if self.LUKS_PART != "":
            str2return += " --luks " + self.LUKS_PART
        if self.PATH_INSTALL != "" :
            if self.REAL_ROOT is not None :
                # REAL_ROOT is defined in debian_conf.py/arch_conf.py
                str2return += " \"" + self.REAL_ROOT + "\""
            else :
                # We show PATH_INSTALL if REAL_ROOT does not exists
                str2return += " \"" + self.PATH_INSTALL + "\""
        if self.PACKAGES != "" :
            str2return += " " + self.PACKAGES
        
        return str2return

    def GetDiskUUID(self) :
        """ Search uuid of root disk (encrypted disk | grub entry) with subprocesses call """
        dsk = ""
        uuid = ""

        if self.LUKS_PART != "" :
            blkid = subprocess.check_output(["blkid | grep {} | sed \"s/.* UUID=//\"".format(self.LUKS_PART)], shell=True)
            uuid = blkid.decode().split("\"")[1]

        return uuid



# BEGIN Localisation
    def SetLocale(self, user_input, arg_locale) :
        
        if not os.path.exists(self.locale.RACINE + self.locale.file_locale) :
            user_input.die("You should have exited before that !\n  -> {} does not exists /!\\", self.locale.RACINE + self.locale.file_locale)
        self.locale.GetLocale(user_input, arg_locale)
        user_input.msg_n("Locale choisie : {}", "32", "32", self.GetLocale())

    def GetLocale(self) :
        if self.locale.LOCALE_IDX > -1 :
            return self.locale.LOCALE[self.locale.LOCALE_IDX]
        return ""
    
    def SetTimeZone(self, user_input, arg_zone) :
        self.locale.GetSubZone(user_input, arg_zone)
        user_input.msg_n("Timezone choisie : {}", "32", "32", self.GetTimeZone())
        
    def GetTimeZone(self) :
        if self.locale.ZONE > -1 :
            return self.locale.TIMEZONE[self.locale.ZONE]
        return ""
    
    def SetKeymap(self, user_input, arg_keymap) :
        self.locale.GetKbdMap(user_input, arg_keymap)
        user_input.msg_n("Keymap choisie : {}", "32", "32", self.GetKeymap())
        
    def GetKeymap(self) :
        if self.locale.KBDMAP_IDX > -1 :
            return self.locale.KBDMAP[self.locale.KBDMAP_IDX]
        return ""
    
    def SetXKbdMap(self, user_input, arg_xkbdmap) :
        self.locale.GetXKbdMap(user_input, arg_xkbdmap)
        user_input.msg_n("XKeymap : {}", "32", "32", self.GetXKbdmap())
    
    def GetXKbdmap(self) :
        if self.locale.XKBDMAP_IDX > -1 :
            return self.locale.XKBDMAP[self.locale.XKBDMAP_IDX]
        return ""
# END Localisation

# BEGIN network config
    def SetNetworkConfig(self, user_input, arg_network) :
        self.reseau.conf_net(user_input, arg_network)
        if self.reseau.NETCONF < len(self.reseau.NETARRAY) - 1 :
            #self.SYSTEMD_ENABLE += " " + self.GetNetworkConfig().lower()
            #print(self.reseau.NETDHCPCD)
            #print(self.reseau.NFSROOT)
            #print(not( self.reseau.NETDHCPCD or self.reseau.NFSROOT))
            
            #print(self.DISTRIB + " " +str(self.reseau.NETDHCPCD))
            if not ( self.reseau.NETDHCPCD or self.reseau.NFSROOT) :
                self.PACKAGES_AUTO += self.GetPackages("pack_net", self.GetNetworkConfig().lower())
                
                self.SYSTEMD_ENABLE += self.GetPackages("systemd_network", self.GetNetworkConfig().lower())
            elif self.reseau.NETDHCPCD :
                # if self.DISTRIB == "ArchLinux" :
                self.SYSTEMD_ENABLE += " " + self.GetNetworkConfig()
                self.PACKAGES_AUTO += self.GetPackages("pack_net", "dhcpcd")
            elif self.reseau.NFSROOT :
                self.NFSROOT = True
                self.PACKAGES_AUTO += self.GetPackages("pack_net", "nfsroot")
                
        user_input.msg_n("Interface réseau choisie : {}", "32", "32", self.GetNetworkConfig())
                
        
    def GetNetworkConfig(self) :
        if self.reseau.NETCONF > -1 :
            return self.reseau.NETARRAY[self.reseau.NETCONF]
        
# END network config

# BEGIN graphic config + desktop environnement + display manager
    def SetGraphicDriver(self, user_input, arg_vid_drv) :
        self.graphic.GetGraphicDriver(user_input, arg_vid_drv)

        if self.graphic.VID_DRIVERS_IDX < len(self.graphic.VID_DRIVERS) - 1 and self.graphic.VID_DRIVERS_IDX > -1 :
            self.PACKAGES_AUTO += self.GetPackages("pack_drv", self.GetVidDriver())
            user_input.msg_n("Graphic driver : {}", "32", "32", self.GetVidDriver())
        if not self.graphic.VID_DRIVERS_IDX > -1 :
            user_input.msg_n("Graphic driver : {}", "32", "32", "None !")
        #elif self.graphic.VID_DRIVERS_IDX == len(self.graphic.VID_DRIVERS) - 2 :
            #self.PACKAGES_AUTO += user_conf.GetPackagesFromKeys(self.DISTRIB, "pack_drv") #user_conf.GetPackagesFromKeys(self.DISTRIB, "pack_drv", vid_drv)
            #for vid_drv in user_conf.GetPackagesFromKeys(self.DISTRIB, "pack_drv") :

#print(user_input.msg_n("Choisie : {}", self.GetVidDriver()) + system_conf.DistConf.DEFAULT_CONF[self.DISTRIB]["pack_drv"][self.GetVidDriver()] )
    #print(graphic.VID_DRIVERS[graphic.VID_DRIVERS_IDX])


        
    def GetVidDriver(self) :
        if self.graphic.VID_DRIVERS_IDX > -1 :
            return self.graphic.VID_DRIVERS[self.graphic.VID_DRIVERS_IDX].lower()
        return ""
    
    def SetEnvironnement(self, user_input, arg_desktop) :
        if self.graphic.VID_DRIVERS_IDX > -1 :
            self.graphic.GetEnvironnement(user_input, arg_desktop)
            if self.graphic.DESKTOP_ENV_IDX < len(self.graphic.DESKTOP_ENV) :
                self.PACKAGES_AUTO += self.GetPackages("pack_environnement", self.GetEnvironement())
                
            if self.graphic.DISPLAYMANAGER_IDX > -1 :
                self.PACKAGES_AUTO += self.GetPackages("pack_displaymanager", self.GetDisplayManager())
                self.SYSTEMD_ENABLE += self.GetPackages("systemd_displaymanager", self.GetDisplayManager())
            user_input.msg_n("Environnement graphique choisi : {} (dm:{})", "32", "32", self.GetEnvironement(), self.GetDisplayManager())
        else :
            user_input.msg_n("{} Skipping graphic environnement.", "32", "31", "/!\\")
        #else :
            
    
    def GetEnvironement(self) :
        if self.graphic.DESKTOP_ENV_IDX > -1 :
            return self.graphic.DESKTOP_ENV[self.graphic.DESKTOP_ENV_IDX]
        return ""
    
    def GetDisplayManager(self) :
        if self.graphic.DISPLAYMANAGER_IDX > -1 :
            return self.graphic.DISPLAYMANAGER[self.graphic.DISPLAYMANAGER_IDX]
        return ""
        
# END graphic config + desktop environnement + display manager


# BEGIN Installation

    def install(self) :
        """
        Main installation function :
        Called from subclass and run functions :
            Install base system
            System configuration
            Run custom bash script 1 (global conf)
            Install additionnal packages
            Add users and passwords, sudo config
            Enable systemd services
            Run custom bash script 1 (user conf)
            Run post install functions
            Grub config for LUKS
            Install grub on disk (bios) | Add boot grub entry (uefi)
            Config kernel for nfsroot
        """
        try :
            # customize system with your own script
            self.EXPORT_BASH = self.EXPORT_BASH.format(self.USER_NAME,self.GRUB_INSTALL, self.GetNetworkConfig(), self.GetEnvironement(), self.GetDisplayManager(), self.GetXKbdmap().split("_")[0], self.DEBIAN_VERS, self.PACK_MANAGER_CACHE_PKG  )

            chroot.run_command.FILE_OUTPUT = "/tmp/distinstall_output"

            # That should not occure
            if self.running == None :
                self.INPUTS.die("Error chroot config !")

            if self.QUIET :
                self.INPUTS.message(self.INPUTS.msg_n.__name__, "Type \"{}\" into another terminal to get output", "32", "32", "tail -f /tmp/distinstall_output")
                
            # BEGIN Base system installation
            if not self.run_install(self.anarchi_base,
                                    ("Installing {}", "base package"), 
                                    ("Base packages {} !", "installed"), 
                                    ("Error install base !", "") ) : 
                raise Exception("Error during base package installation !")
            # END Base system installation
            # BEGIN System configuration (langage, fstab, timezone...)
            if not self.run_install(self.anarchi_conf,
                                    ("Configuring {}", "system"), 
                                    ("{} configuration...done !", "System"), 
                                    ("Error config !", "") ) : 
                raise Exception("Error during system configuration !")
            # END config
            
            # BEGIN Custom
            # if self.DISTRIB != "gentoo" :
            self.run_install(self.anarchi_custom,
                            ("Execution du script utilisateur : {}", user_conf.CUSTOM_SCRIPT),
                            ("{} has done successfully !", user_conf.CUSTOM_SCRIPT),
                            ("Error occured while running {} !", user_conf.CUSTOM_SCRIPT) )
            # END

            # BEGIN customize system with your own script with users commands
            if self.DISTRIB == "gentoo" :
                self.run_install(self.anarchi_kernel_compil,
                                ("Kernel compilation...", ""),
                                ("Kernel compilation {}", "32", "32", "ok"),
                                ("Kernel compilation {}", "failed !"))
            # END custom

            # BEGIN Packages installation 
            # Searching for missing package
            self.INPUTS.message(self.INPUTS.msg_nn2.__name__, "Checking packages : {}".format(", ".join(self.PACKAGES.strip().split(" "))))
            err_pkg = self.SearchPackage(self.PACKAGES, True)
            if err_pkg != "" :
                self.INPUTS.caution("{} has not been installed !", err_pkg)
            self.INPUTS.clear_line()
            # Install packages
            if not self.run_install(self.anarchi_packages,
                                    ("Installing packages", ""),
                                    ("Packages {} !", "installed"), 
                                    ("Error install packages !", "") ) : 
                raise Exception("Error during packages installation !")
            # END Packages installation
            
            # BEGIN Useradd
            self.run_install(self.anarchi_passwd,
                             ("Adding user : {}", self.USER_NAME),
                             ("Utilisateur {} ajouté", self.USER_NAME), 
                             ("Error passwd {} !", self.USER_NAME))
            # END Useradd
            
            # BEGIN SYSTEMD enable
            if self.SYSTEMD_ENABLE.strip() != "" :
                self.run_install(self.anarchi_systemd,
                                 ("Systemd enable : \"{}\"", self.SYSTEMD_ENABLE.strip()), 
                                 ("Systemd enabled : \"{}\"", self.SYSTEMD_ENABLE.strip()), 
                                 ("Error enabling systemd {} !", self.SYSTEMD_ENABLE.strip()))
            # END System
            
            
            # Mount cache package if its not default cache
            if self.PACK_MANAGER_CACHE_PKG != "" :
                self.running.add_mount(self.PATH_INSTALL + self.PACK_MANAGER_CACHE_DEFAULT, self.PACK_MANAGER_CACHE_PKG, "none", "bind")                 
            
            # Mount resolv.conf to get same ethernet used on hosts
            if not self.running.add_mount(self.PATH_INSTALL + "/etc/resolv.conf", "/etc/resolv.conf", "none", "bind") :
                self.INPUTS.die("Unable to mount resolv!")
            
            # BEGIN Post install 
            self.run_install(self.anarchi_post_install,
                             ("Post installation...",  ""), 
                             ("Post installation... done", ""), 
                             ("Error occured while Init {} & {}...",  "GPG", "pacman"))
            # END Post install 
            
            # BEGIN customize system with your own script with users commands
            # if self.DISTRIB != "gentoo" :
            self.custom_user_script_path = os.path.join(user_conf.CUSTOM_SCRIPT_DIR, self.USER_NAME)
            self.run_install(self.anarchi_custom_user,
                            ("Execution du script utilisateur : {}", self.custom_user_script_path),
                            ("{} has done successfully !", "32", "32", self.custom_user_script_path),
                            ("Error occured while running {} !", self.custom_user_script_path))
            # END custom

            if self.PLYMOUTH :
                self.run_install(self.anarchi_plymouth,
                                ("{} configuration", "Plymouth"),
                                ("{} has been successfully configured !", "32", "32", "Plymouth"),
                                ("Error occured while configuring {} !", "plymouth"))

            # BEGIN Install grub
            if self.GRUB_INSTALL != "" or self.GRUB_EFI :

                if not self.TEST_MODE and not os.path.exists( self.PATH_INSTALL + self.GRUB_BIN ) :
                    self.INPUTS.caution("{} not installed !", "Grub")
                    #self.running.add_chroot(self.PACK_MANAGER_INSTALL + self.GetPackages("pack_grub"), self.PATH_INSTALL)
                    if self.GRUB_EFI :
                        self.running.add_chroot(self.PACK_MANAGER_INSTALL + self.GetPackages("pack_grub_efi"), self.PATH_INSTALL)
                    else :
                        self.running.add_chroot(self.PACK_MANAGER_INSTALL + self.GetPackages("pack_grub"), self.PATH_INSTALL)
                    
                if self.LUKS_PART != "":
                    self.run_install(self.anarchi_luks,
                                    ("Configuration de GRUB pour \"{}\"", "LUKS"),
                                    ("{} configured for {}", "Grub", "LUKS"),
                                    ("Error occured while configuring grub for {} !", "LUKS"))

                self.run_install(self.anarchi_grub,
                                 ("Installation de grub sur le disque \"{}\"", self.GRUB_INSTALL),
                                 ("{} installed on {}", "Grub", self.PATH_INSTALL),
                                 ("Error occured while installing grub on {} !", self.GRUB_INSTALL))
            # END Grub
            
            #self.INPUTS.die("Test Gentoo {}", "OK")
            # BEGIN Nfsroot config
            if self.NFSROOT :
                self.run_install(self.anarchi_nfs_root,
                                 ("Execution des commandes pour \"{}\"", "nfsroot"), 
                                 ("System is ready to be runned on {}", "nfsroot"), 
                                 ("Error occured while creating {} installation !", "nfsroot"))
            # END Nfsroot config
                
            if err_pkg != "" :
                self.INPUTS.caution("{} has not been installed !", err_pkg)
        except Exception as e :
            #chroot.chroot_init.umount_disk()
            self.INPUTS.error("{}\n", str(e))
        finally :
            time.sleep(0.2)
            chroot.chroot_init.umount_disk()
            if len(chroot.chroot_init.UMOUNT_FAILED) > 0 :
                msg_fail = ""
                for failed_mount in chroot.chroot_init.UMOUNT_FAILED :
                    msg_fail += " " +failed_mount
                    
                self.INPUTS.caution("umount {} failed !", msg_fail)
                if self.INPUTS.rid_continue("Force umount {} ?", msg_fail) :
                    if not chroot.run_command.run("umount " + msg_fail) == 0 :
                        self.INPUTS.caution("umount {} failed again !\n  -> Run command below to force umount if you really know what you're doing !\numount -l" + msg_fail, msg_fail)
            else :
                self.INPUTS.message(self.INPUTS.msg_n.__name__, "unmount chroot...done", "32", "32")

            self.LIST_CMD = self.running.GetListChroot()
            #print(self.running.LIST_EXEC)

    def run_install(self, func, msg_run, msg_ok, msg_fail, option_user = "") :
        """
        Run functions to install distribution
        func : function to run
        msg_run : Message to display before running
        msg_ok : Message to display after successfully run
        msg_fail : Message to display if function failed
        option_user : Options to give for particular function
        """
        already_done = os.path.exists("/tmp/done/" + func.__name__)
        self.INPUTS.info(*msg_run)
        
        if self.TEST_MODE or not self.QUIET :
            print()
        returncode =  func()

        self.INPUTS.clear_line()
        if already_done :
            self.INPUTS.message(self.INPUTS.msg_n.__name__, "{} | " + msg_ok[0], "32", "33", "already done", *msg_ok[1:])
        else :            
            if not returncode :
                self.INPUTS.error(*msg_fail)
            else :
                self.INPUTS.success(*msg_ok)

        return returncode

    # def add_install(self) :

                    
# BEGIN ANARCHI 
    def anarchi_pre_install(self) :
        return True
    
    def anarchi_passwd(self) : #, path_chroot = "") :
        if self.OVERRIDE_PATH != "" :
            self.running.add_chroot(user_conf.USER_CMD_CHROOT.format("bash", self.OVERRIDE_PATH + "\n" + user_conf.USER_CMD["useradd"].format(self.OPTPASSWD, self.USER_NAME)))
            cmd_passwd = user_conf.USER_CMD_CHROOT.format("bash", user_conf.USER_CMD_PASSWD)
            #self.running.add_chroot(user_conf.USER_CMD["useradd"].format("-m -g users" + option_user, self.USER_NAME), self.REAL_ROOT) 
        else :
            self.running.add_chroot(user_conf.USER_CMD["useradd"].format(self.OPTPASSWD, self.USER_NAME), self.PATH_INSTALL) 
            cmd_passwd  = user_conf.USER_CMD_CHROOT.format("bash", user_conf.USER_CMD_PASSWD)
            
        if not self.TEST_MODE :
            # We create file in new installed system, it will be destroy after umounting the new system.
            with open(self.PATH_INSTALL + "/tmp/passwd", "w") as outfile :
                outfile.write(self.OVERRIDE_PATH + "\n")
                for key_user in self.USERS :
                    #print(self.OVERRIDE_PATH + "\n" + cmd_passwd.format(key_user, self.USERS[key_user]))
                    outfile.write(cmd_passwd.format(key_user, self.USERS[key_user]) + "\n")

        #for key_user in self.USERS :
            #print(self.OVERRIDE_PATH + "\n" + cmd_passwd.format(key_user, self.USERS[key_user]))
        self.running.add_chroot("bash /tmp/passwd", self.PATH_INSTALL)
        # self.running.run_chroot("cat /tmp/passwd", self.PATH_INSTALL)
        #self.running.run_chroot( "bash")
        if self.SUDO :
            #self.SetSudo()
            CMD_CREATE = user_conf.USER_CMD["sudoers"].format(self.PATH_INSTALL, self.USER_NAME, self.PATH_INSTALL, self.USER_NAME)
            res_cmd = self.running.add(CMD_CREATE)
            self.running.add_chroot(user_conf.USER_CMD["lock_account"].format("root"), self.PATH_INSTALL)
        #else :
            #self.running.add_chroot("bash /tmp/passwd " + "root " + self.ROOT_PASSWD, self.PATH_INSTALL)

        res_cmd = self.running.run_all_once( self.anarchi_passwd.__name__)
        return res_cmd == 0
    
    def anarchi_systemd(self) :
        res = 0
        for systd in self.SYSTEMD_ENABLE.split(" ") :
            if systd.strip() != "" :
                self.running.add_chroot(user_conf.SYSTEMD_SERVICE.format(systd.strip()), self.PATH_INSTALL)
            #print(user_input.msg_nn("Systemd enabling \"{}\"", self.SYSTEMD_ENABLE))
        #res = self.running.add(self.PATH_INSTALL, user_conf.user_conf.SYSTEMD_SERVICE.format(systd.strip()))
        res = self.running.run_all_once( self.anarchi_systemd.__name__)
            #user_input.error("Error systemd enable {}", systd)
                #res = 1
        return res == 0
    
    def anarchi_grub(self, override_path_install = True) :
        try :
            #DEFAULT_UPDATE = user_conf.GRUB_CMD["update_ArchLinux"]
            CMD_CREATE = user_conf.GRUB_CMD["power"].format(self.PATH_INSTALL)
            #res_cmd = chroot.run_command.run(CMD_CREATE)
            self.running.add( CMD_CREATE )
            
            CMD_INSTALL = user_conf.GRUB_CMD["install"].format(self.GRUB_INSTALL)
            # Gentoo needs /usr/sbin in his path when installing from Archlinux
            if override_path_install :
                CMD_INSTALL = user_conf.USER_CMD_CHROOT.format("bash", self.OVERRIDE_PATH + "\n" + user_conf.GRUB_CMD["install"].format(self.GRUB_INSTALL))

            if self.GRUB_EFI :
                CMD_INSTALL = user_conf.GRUB_CMD["install_efi"].format(self.hostname)
            else :
                CMD_INSTALL = user_conf.GRUB_CMD["install"].format(self.GRUB_INSTALL)
            # Gentoo needs /usr/sbin in his path when installing from Archlinux
            if override_path_install :
                CMD_INSTALL = user_conf.USER_CMD_CHROOT.format("bash", self.OVERRIDE_PATH + "\n" + CMD_INSTALL)

            self.running.add_chroot(CMD_INSTALL, self.PATH_INSTALL)
            
            CMD_UPDATE = user_conf.GRUB_CMD["update_" + self.DISTRIB]
            
            # Debian needs /usr/sbin in his path when installing from Archlinux
            if override_path_install :
                CMD_UPDATE = user_conf.USER_CMD_CHROOT.format("bash", self.OVERRIDE_PATH + "\n" + user_conf.GRUB_CMD["update_" + self.DISTRIB])
            
            self.running.add_chroot(CMD_UPDATE, self.PATH_INSTALL)
            return self.running.run_all_once( self.anarchi_grub.__name__ ) == 0

        except Exception as e :
            print(self.running.LIST + "\n" + str(e))
            return False

    def anarchi_custom(self) :
        override_path = ""
        if self.OVERRIDE_PATH != "" :
            override_path = self.OVERRIDE_PATH + "\n"
            
        # Copy custom.d directory to new root
        chroot.run_command.run("cp -RL custom.d {}/tmp/".format(self.PATH_INSTALL))
        if not self.INTERACTIVE:
            self.running.add_chroot(user_conf.USER_CMD_CHROOT.format("bash", override_path + self.EXPORT_BASH + "\nbash " + user_conf.CUSTOM_SCRIPT), self.PATH_INSTALL)

            return self.running.run_all_once(self.anarchi_custom.__name__) == 0
        else :
            self.INPUTS.msg_n("Welcome to your new {} installation {}", "32", "32", self.DISTRIB, "/!\\")
            if override_path != "" :
                self.INPUTS.msg_n2("Type following command to set PATH :\n", self.OVERRIDE_PATH)
            self.running.run_chroot()
            return True

    def anarchi_custom_user(self) :
        """ Run custom scripts named as main user """
        # Disable quiet commands for custom scripts (ssh-keygen, ssh-copy-id, ...)
        quiet = chroot.run_command.QUIET_MODE
        chroot.run_command.QUIET_MODE = False

        override_path = ""
        if self.OVERRIDE_PATH != "" :
            override_path = self.OVERRIDE_PATH + ";"
        # self.running.add_chroot(user_conf.USER_CMD_CHROOT.format("bash", override_path + self.EXPORT_BASH + "\nbash /tmp/custom.d/custom " +  self.USER_NAME), self.PATH_INSTALL)
        self.running.add(override_path + self.EXPORT_BASH + "; chroot " + self.PATH_INSTALL + " bash " + self.custom_user_script_path)
        res_cmd = self.running.run_all_once(self.anarchi_custom_user.__name__)
        # Enable quiet commands (or not...)
        chroot.run_command.QUIET_MODE = quiet
        return res_cmd == 0

    def anarchi_plymouth(self) :
        # cmd_plymouth = user_conf.GRUB_CMD["plymouth_config"].format(" splash", self.PATH_INSTALL)

        cmd_plymouth = user_conf.GRUB_CONFIG.format(" splash", self.PATH_INSTALL)
        self.running.add(cmd_plymouth)

        user_conf.HOOKS_LUKS_MKINITCPIO[0] = "plymouth " + user_conf.HOOKS_LUKS_MKINITCPIO[0]

        return self.running.run_all_once(self.anarchi_plymouth.__name__) == 0

    def anarchi_luks(self) :
        # GRUB/Luks config
        double_quote_escape = "\"".replace("\"", "\\\"")
        crypt_grub = "cryptdevice=UUID={}:root-{} root=\\/dev\\/mapper\\/root-{}".format(self.UUID_ROOT, self.hostname, self.hostname)
        if self.PLYMOUTH :
            crypt_grub += " splash"
        cmd_luks = user_conf.GRUB_CONFIG.format(crypt_grub, self.PATH_INSTALL)
        # cmd_luks = user_conf.GRUB_CMD["luks_config"].format(double_quote_escape + crypt_grub + double_quote_escape, self.PATH_INSTALL)
        self.running.add(cmd_luks)

    def anarchi_nfs_root(self, a_nfs_root) :
        for cmd_nfsroot in a_nfs_root :
            self.running.add_chroot(cmd_nfsroot, self.PATH_INSTALL)

        return self.running.run_all_once( self.anarchi_nfs_root.__name__ ) == 0
    
    def anarchi_post_install(self) :
        return True
#  END ANARCHI

# END Installation

