# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# coding: utf-8
import subprocess
import locale

class Localisation :
    def __init__(self):
        self.RACINE = ""
        # Time zone
        self.TIMEZONE = []
        self.ZONE = -1
        self.TIMEZONE_LIB = ""
        # Locale 
        self.LOCALE = []
        self.LOCALE_IDX = -1
        self.LOCALE_LIB = ""
        # Keyboard map
        self.KBDMAP = []
        self.KBDMAP_IDX = -1
        self.KBDMAP_LIB = ""
        # Keyboard map in X
        self.XKBDMAP = []
        self.XKBDMAP_IDX = -1
        self.XKBDMAP_LIB = ""
        
        self.file_zoneinfo = "/usr/share/zoneinfo/zone.tab"
        self.file_kbdmap = "/usr/share/kbd/keymaps"
        self.file_kbdmapext = "map.gz"
        #self.file_xkbdmap = ""
        self.file_locale = "/etc/locale.gen"
    
    
    def GetTimeZones(self) :
        ZONES = list()
        # Get timezones from /usr/share/zoneinfo/zone.tab 
        output = subprocess.check_output(['cat ' + self.file_zoneinfo + ' | awk \'{print $3}\' | grep "/" | sed "s/\\/.*//g" | sort -ud' ], shell=True)
        result = output.decode().strip().split('\n')
        for zone in result :
            if zone != "" :
                ZONES.append(zone)
                
        return ZONES
    def GetSubZone(self) :
        if self.ZONE > -1 :
            return self.TIMEZONE[self.ZONE]
    def GetSubZone(self, user_input, arg_zone = "") :
        zones = self.GetTimeZones()
        if arg_zone != "" :
            zone = arg_zone.split("/")[0]
            if zone not in zones :
                choix_zone = zones[user_input.rid_menu( "Choisir", {"column" : True, "0" : "Zone"}, *zones) - 1]
            else :
                choix_zone = arg_zone.replace(zone + "/", "")
        else :
            choix_zone = zones[user_input.rid_menu( "Choisir", {"column" : True, "0" : "Zone"}, *zones) - 1]
            
        SUBZONES = list()
        # Get ethernet interfaces throught lspci
        output = subprocess.check_output(['cat ' + self.file_zoneinfo + ' | awk \'{print $3}\' | grep "/" | sort -ud | grep '+ choix_zone ], shell=True)
        
        result = output.decode().strip().split('\n')
        if arg_zone not in result :
            for iface in result :
                if iface != "" :
                    SUBZONES.append(iface.replace(choix_zone+ "/" ,""))
            yo = user_input.rid_menu( "Choisir", {"column" : True}, *SUBZONES)
            self.ZONE = yo - 1
            self.TIMEZONE = result
        else :
            self.TIMEZONE = [ arg_zone ]
            self.ZONE = 0
        #retur  n yo - 1

    def GetKbdMap(self, user_input, arg_kbdmap = "") :
        #KBD_DIR="/usr/share/kbd/keymaps"
        #KBD_EXT="map.gz"
        #print(arg_kbdmap)
        output = subprocess.check_output(['ls -R '+ self.file_kbdmap + ' | grep ' + self.file_kbdmapext + ' | sed "s/\\.' + self.file_kbdmapext + '//g" | sort' ], shell=True)
        
        result = output.decode().strip().split('\n')
        if arg_kbdmap not in result :
            for iface in result :
                if iface != "" :
                    self.KBDMAP.append(iface)
            yo = user_input.rid_menu( "Choisir", {"column" : True}, *self.KBDMAP)
            self.KBDMAP_IDX = yo - 1
            #self.KBDMAP = result
        else :
            self.KBDMAP = [ arg_kbdmap ]
            self.KBDMAP_IDX = 0

    def GetXKbdMap(self, user_input, arg_kbdmap = "") :
        output = "af_Afghani al_Albanian am_Armenian ara_Arabic at_German-Austria az_Azerbaijani ba_Bosnian bd_Bangla be_Belgian bg_Bulgarian br_Portuguese-Brazil bt_Dzongkha bw_Tswana by_Belarusian ca_French-Canada cd_French-DR-Congo ch_German-Switzerland cm_English-Cameroon cn_Chinese cz_Czech de_German dk_Danishee_Estonian epo_Esperanto es_Spanish et_Amharic fo_Faroese fi_Finnish fr_French gb_English-UK ge_Georgian gh_English-Ghana gn_French-Guinea gr_Greek hr_Croatian hu_Hungarian ie_Irish il_Hebrew iq_Iraqi ir_Persian is_Icelandic it_Italian jp_Japanese ke_Swahili-Kenya kg_Kyrgyz kh_Khmer-Cambodia kr_Korean kz_Kazakh la_Lao latam_Spanish-Lat-American lk_Sinhala-phonetic lt_Lithuanian lv_Latvian ma_Arabic-Morocco mao_Maori md_Moldavian me_Montenegrin mk_Macedonian ml_Bambara mm_Burmese mn_Mongolian mt_Maltese mv_Dhivehi ng_English-Nigeria nl_Dutch no_Norwegian np_Nepali ph_Filipino pk_Urdu-Pakistan pl_Polish pt_Portuguese ro_Romanian rs_Serbian ru_Russian se_Swedish si_Slovenian sk_Slovak sn_Wolof sy_Arabic-Syria th_Thai tj_Tajik tm_Turkmen tr_Turkish tw_Taiwanese tz_Swahili-Tanzania ua_Ukrainian us_English-US uz_Uzbek vn_Vietnamese za_English-S-Africa"
        
        result = output.strip().split(' ')
        if arg_kbdmap not in result :
            for iface in result :
                if iface != "" :
                    self.XKBDMAP.append(iface)
                    if iface.split("_")[0] == arg_kbdmap :
                        self.XKBDMAP_IDX = len(self.XKBDMAP) - 1
                        return
            yo = user_input.rid_menu( "Choisir", {"column" : True}, *self.XKBDMAP)
            self.XKBDMAP_IDX = yo - 1
            #self.XKBDMAP = result
        else :
            self.XKBDMAP = [ arg_kbdmap ]
            self.XKBDMAP_IDX = 0

    def GetLocale(self, user_input, arg_locale = "") :
        SUBZONES = list()
        
        # On commence par récupérer les types de locales        
        cmd_locales = 'cat "' + self.RACINE + self.file_locale + '" | grep -v "#  " | sed \'s/#//g\' | sed \'s/ UTF-8//g\' | grep .UTF-8'
        output = subprocess.check_output([cmd_locales], shell=True)
        
        result = output.decode().strip().replace(" ","").split('\n')
        cur_locale = locale.getdefaultlocale()
        cur_locale = cur_locale[0] + "." + cur_locale[1]
        if arg_locale in result or arg_locale + ".UTF-8" in result :
            self.LOCALE = [ arg_locale ]
            self.LOCALE_IDX = 0
        else :
            if cur_locale in result :
                if user_input.rid_continue("Utiliser {}", cur_locale) :
                    self.LOCALE = [ cur_locale ]
                    self.LOCALE_IDX = 0
                    return 0
            for iface in result :
                if iface != "" :
                    SUBZONES.append(iface)
            yo = user_input.rid_menu( "Choisir", {"column" : True}, *SUBZONES)
            self.LOCALE_IDX = yo - 1
            self.LOCALE = result
            



    #def GetLocale(self, user_input, arg_locale = "") :
        #Locales = list()
        #Charsets = list()
        
        ##cat "/etc/locale.gen" | grep "#".*_ | awk '{print $2}'
        #cmd_locales = 'cat "' + self.RACINE + self.file_locale + '" | grep "#".*_ | awk \'{print $1}\''
        ## On commence par récupérer les types de locales
        
        ##cmd_locales = 'cat "' + self.RACINE + self.file_locale + '" | grep -v "#  " | sed \'s/#//g\' | sed \'s/ UTF-8//g\' | grep .UTF-8'
        #output = subprocess.check_output([cmd_locales], shell=True)
        ##print(cmd_locales)
        
        
        ##output = subprocess.check_output(['cat "' + self.RACINE + self.file_locale + '" | grep -v "#  " | sed \'s/#//g\' | sed \'s/ UTF-8//g\' | grep .UTF-8'  ], shell=True)
        
        #result = output.decode().strip().replace(" ","").split('\n')
        #cur_locale = locale.getdefaultlocale()
        #cur_locale = cur_locale[0] + "." + cur_locale[1]
        #if arg_locale in result in result :
            #self.LOCALE = [ arg_locale ]
            #self.LOCALE_IDX = 0
        #else :
            #if cur_locale in result :
                #if user_input.rid_continue("Utiliser {}", cur_locale) :
                    #self.LOCALE = [ cur_locale ]
                    #self.LOCALE_IDX = 0
                    #return 0
                
            #cmd_locales = 'cat "' + self.RACINE + self.file_locale + '" | grep "#".*_ | awk \'{print $2}\''
            #output = subprocess.check_output([cmd_locales], shell=True)
            #result = output.decode().strip().replace(" ","").split('\n')
            #for iface in result :
                #if iface != "" and iface not in Charsets:
                    #Charsets.append(iface)
            #yo = user_input.rid_menu( "Choisir Charset", {"column" : True}, *Charsets)
            #cmd_locales = 'cat "' + self.RACINE + self.file_locale + '" | grep "#".*_ | grep "' + Charsets[yo - 1] + '" | awk \'{print $1}\''
            #output = subprocess.check_output([cmd_locales], shell=True)
            #result = output.decode().strip().replace(" ","").split('\n')
            #for iface in result :
                #if iface != "" :
                    #Locales.append(iface)
            #yo = user_input.rid_menu( "Choisir Charset", {"column" : True}, *Locales)

            
            
            #self.LOCALE_IDX = yo - 1
            #self.LOCALE = result
            

