# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# coding: utf-8
class EN_GRAPHICS :
    NVIDIA340 = [ "nvidia-340xx", "nv340" ]
    NVIDIA304 = [ "nvidia-304xx", "nv304" ]
    NVIDIA = "nvidia"
    VB = [ "VirtualBox", "vb", "virtualbox"]
    INTEL = "intel"
    NOUVEAU = ["nouveau", "nv"]
    AMD = "amdgpu"
    RADEON = "radeon"
    ALL = "Tous"
    NOTHING = "Aucun"
    #"NetworkManager", "Connman", "Dhcpcd", "Nfsroot (PXE)", "Rien (TODO...)"
    def GetArrayNvidia() :
        return [ EN_GRAPHICS.NVIDIA304[0], EN_GRAPHICS.NVIDIA340[0], EN_GRAPHICS.NVIDIA ]
    
    def GetArray() :
        return [ EN_GRAPHICS.AMD.capitalize(), EN_GRAPHICS.RADEON.capitalize(), EN_GRAPHICS.NOUVEAU[0].capitalize(),  EN_GRAPHICS.INTEL.capitalize(), EN_GRAPHICS.NVIDIA.capitalize(), EN_GRAPHICS.VB[0], EN_GRAPHICS.ALL, EN_GRAPHICS.NOTHING ]

class EN_ENVIRONNEMENT :
    PLASMA = "plasma"
    GNOME = "gnome"
    MATE = "mate"
    CINNAMON = "cinnamon"
    XFCE = "xfce"
    LXDE = "lxde"
    LXQT = "lxqt"
    ENLIGHTENMENT = "enlightenment"
    FLUXBOX = "fluxbox"
    DEEPIN = "deepin"
    BUDGIE_DESKTOP = "budgie-desktop"
    I3 = "i3"
    SWAY = "sway"
    
    
    def GetArray() :
        return [ EN_ENVIRONNEMENT.PLASMA, EN_ENVIRONNEMENT.GNOME, EN_ENVIRONNEMENT.MATE, EN_ENVIRONNEMENT.CINNAMON, EN_ENVIRONNEMENT.XFCE, EN_ENVIRONNEMENT.LXDE, EN_ENVIRONNEMENT.LXQT, EN_ENVIRONNEMENT.ENLIGHTENMENT, EN_ENVIRONNEMENT.FLUXBOX, EN_ENVIRONNEMENT.DEEPIN, EN_ENVIRONNEMENT.BUDGIE_DESKTOP, EN_ENVIRONNEMENT.I3, EN_ENVIRONNEMENT.SWAY ]
    
    def GetArrayDM() :
        return { EN_ENVIRONNEMENT.PLASMA : EN_DISPLAYMANAGER.SDDM, EN_ENVIRONNEMENT.GNOME : EN_DISPLAYMANAGER.GDM, EN_ENVIRONNEMENT.MATE : EN_DISPLAYMANAGER.LIGHTDM, EN_ENVIRONNEMENT.CINNAMON : EN_DISPLAYMANAGER.LIGHTDM, EN_ENVIRONNEMENT.XFCE : EN_DISPLAYMANAGER.LIGHTDM, EN_ENVIRONNEMENT.LXDE : EN_DISPLAYMANAGER.LXDM, EN_ENVIRONNEMENT.LXQT : EN_DISPLAYMANAGER.SDDM, EN_ENVIRONNEMENT.ENLIGHTENMENT : EN_DISPLAYMANAGER.LIGHTDM, EN_ENVIRONNEMENT.FLUXBOX : EN_DISPLAYMANAGER.SLIM, EN_ENVIRONNEMENT.DEEPIN : EN_DISPLAYMANAGER.LIGHTDM, EN_ENVIRONNEMENT.BUDGIE_DESKTOP : EN_DISPLAYMANAGER.LIGHTDM, EN_ENVIRONNEMENT.I3 : EN_DISPLAYMANAGER.SLIM, EN_ENVIRONNEMENT.SWAY : EN_DISPLAYMANAGER.SLIM }
        
class EN_DISPLAYMANAGER :
    SDDM = "sddm" 
    GDM = "gdm" 
    LIGHTDM = "lightdm" 
    LXDM = "lxdm" 
    SLIM = "slim" 
    XDM = "xdm" 
    NODM = "nodm"
    
    def GetArray() :
        return [ EN_DISPLAYMANAGER.SDDM, EN_DISPLAYMANAGER.GDM, EN_DISPLAYMANAGER.LIGHTDM, EN_DISPLAYMANAGER.LXDM, EN_DISPLAYMANAGER.SLIM, EN_DISPLAYMANAGER.XDM, EN_DISPLAYMANAGER.NODM ]

class DistConf :
    MOD_NFSROOT = "nfsv4 atl1c forcedeth 8139too 8139cp r8169 e1000 e1000e broadcom tg3 sky2"
    PACK_MANAGER = {
        "apt" : {
            "search" : "search",
            "install" : "install",
            "update" : "update",
            "upgrade" : "upgrade",
            "noconfirm" : "-y",
            },
        "yum" : {
            "search" : "search",
            "install" : "install"  ,
            "update" : "update"    ,
            "upgrade" : "upgrade"  ,
            "noconfirm" : "-y"      ,
            },
        "pacman" : {
            "search" : "-Ss"       ,
            "install" : "-S"      ,
            "update" : "-Sy"       ,
            "upgrade" : "Syu"      ,
            "noconfirm" : "-y"      ,
            },
        "emerge" : {
            "search" : "-s"       ,
            "install" : "",
            "update" : ""       ,
            # "upgrade" : "--sync"      ,
            "noconfirm" : "-y"      ,
            }
        }
    ARCHS = {   "amd64" : ["x86_64"],
                "i386"  : ["x86", "x86_64"]
            }
    DEFAULT_CONF = {
        #"grub" : ("grub", "os-prober")
        "ArchLinux" : {
            "pack_man" : "pacman",
            "pack_base" : ("base", "base-devel"),
            #"flashplugin", "icedtea-web", "jre7-openjdk
            "pack_common" : ("openssh", "nfs-utils", "zip", "unzip", "unrar", "extra/ntfs-3g", "core/dosfstools", "nano"),
            "pack_desktop" : ("xorg-server", "vlc", "libx264", "noto-fonts", "gst-plugins-bad", "gst-plugins-base", "gst-plugins-good", "gst-plugins-ugly"),
            "pack_grub" : ("grub", "os-prober"),
            "pack_grub_efi" : ("grub", "os-prober", "efibootmgr"),
            "pack_syslinux" : ("syslinux"),
            "pack_lang" : ("noto-fonts-cjk"),
            "pack_nav"  : ("firefox"),
            "pack_drv" : {
                "amdgpu"     : ["xf86-video-amdgpu"],
                "radeon"     : ["xf86-video-ati"],
                "intel"      : ["xf86-video-intel"],
                "virtualbox" : ["virtualbox-guest-utils", "xf86-video-vmware" ], #, "virtualbox-guest-modules-arch"],
                "nouveau"    : ["xf86-video-nouveau"],
                "tous"       : ["xf86-video-amdgpu", "xf86-video-ati", "xf86-video-intel", "xf86-video-nouveau", "virtualbox-guest-utils", "xf86-video-vmware"],
                "nvidia"     : ["nvidia nvidia-libgl nvidia-utils"],
                "nvidia-340xx":["nvidia-340xx", "nvidia-340xx-libgl", "nvidia-340xx-utils"],
                "nvidia-304xx":["nvidia-304xx", "nvidia-304xx-libgl", "nvidia-304xx-utils"],
                }, 
            "pack_net" : {
                "networkmanager"       : ("networkmanager"),   
                "connman"       : ("connman"),      
                "dhcpcd"     : ("dhcpcd"),      
                "nfsroot"     : ("mkinitcpio-nfs-utils")
                }, 
            "pack_environnement" : {
                EN_ENVIRONNEMENT.PLASMA : ("konsole", "dolphin", "kate", "gwenview", "okular", "strawberry", "ark", "kfind", "systemsettings", "kcalc", "dvd+rw-tools cdrdao k3b", "breeze", "kscreen", "kwin", "powerdevil", "phonon-qt5-gstreamer", "plasma-desktop", "plasma-workspace"),
                EN_ENVIRONNEMENT.GNOME : ("gnome-terminal", "gnome-control-center", "gnome-system-monitor", "nautilus", "brasero", "gedit", "eog", "gnome-calculator", "gvfs gvfs-smb", "aisleriot", "gnome-shell"),
                EN_ENVIRONNEMENT.MATE : ("mate-terminal", "caja", "pluma", "engrampa", "eom", "atril", "mate-control-center", "gvfs", "mate-session-manager", "mate-themes", "mate-panel"), 
                EN_ENVIRONNEMENT.CINNAMON : (), 
                EN_ENVIRONNEMENT.XFCE : (), 
                EN_ENVIRONNEMENT.LXDE : ("lxterminal", "pcmanfm", "leafpad", "gpicview", "gvfs", "gvfs-smb", "community/epdfview", "lxde-icon-theme", "lxrandr", "openbox", "lxappearance", "lxappearance-obconf", "lxde-common", "lxinput", "lxlauncher", "lxmenu-data", "lxpanel", "lxsession", "lxtask"), 
                EN_ENVIRONNEMENT.LXQT : ("lxqt-config", "lxqt-globalkeys", "lxqt-notificationd", "lxqt-openssh-askpass", "lxqt-panel", "lxqt-policykit", "lxqt-qtplugin", "lxqt-runner", "lxqt-session", "openbox", "pcmanfm-qt", "qterminal", "xcompmgr","breeze" , "breeze-icons"), 
                EN_ENVIRONNEMENT.ENLIGHTENMENT : (), 
                EN_ENVIRONNEMENT.FLUXBOX : ("xterm", "feh", "xcompmgr", "fluxbox"),
                EN_ENVIRONNEMENT.DEEPIN : (), 
                EN_ENVIRONNEMENT.BUDGIE_DESKTOP : (), 
                EN_ENVIRONNEMENT.I3 : (), 
                EN_ENVIRONNEMENT.SWAY : ("sway"),
                }, 
            "pack_displaymanager" : {
                EN_DISPLAYMANAGER.SDDM : (EN_DISPLAYMANAGER.SDDM),
                EN_DISPLAYMANAGER.GDM : (EN_DISPLAYMANAGER.GDM),
                EN_DISPLAYMANAGER.LIGHTDM : (EN_DISPLAYMANAGER.LIGHTDM, EN_DISPLAYMANAGER.LIGHTDM + "-gtk-greeter"),
                EN_DISPLAYMANAGER.LXDM : (EN_DISPLAYMANAGER.LXDM),
                EN_DISPLAYMANAGER.SLIM : (EN_DISPLAYMANAGER.SLIM),
                EN_DISPLAYMANAGER.XDM : (EN_DISPLAYMANAGER.XDM),
                EN_DISPLAYMANAGER.NODM : (EN_DISPLAYMANAGER.NODM)
                }, 
            "pack_cups"      : ("cups"),
            "pack_hplip"      : ("hplip"),
            "pack_bluez"      : ("bluez", "bluez-utils", "bluez-tools"),
            "pack_office"      : ("libreoffice-still"),
            "pack_mail"      : ("thunderbird"),
            "pack_mesa"      : ("mesa"),
            "pack_wifi" : ( "wpa_supplicant", "iw"), 
            "pack_tool_netm" : ("network-manager-applet"), 
            #"systemd_enable" : ("nfs-client.target", "sshd"),
            "systemd_enable" : ("nfs-client.target", "sshd"), 
            "systemd_displaymanager" : {
                EN_DISPLAYMANAGER.SDDM : (EN_DISPLAYMANAGER.SDDM),
                EN_DISPLAYMANAGER.GDM : (EN_DISPLAYMANAGER.GDM),
                EN_DISPLAYMANAGER.LIGHTDM : (EN_DISPLAYMANAGER.LIGHTDM),
                EN_DISPLAYMANAGER.LXDM : (EN_DISPLAYMANAGER.LXDM),
                EN_DISPLAYMANAGER.SLIM : (EN_DISPLAYMANAGER.SLIM),
                EN_DISPLAYMANAGER.XDM : (EN_DISPLAYMANAGER.XDM),
                EN_DISPLAYMANAGER.NODM : (EN_DISPLAYMANAGER.NODM)
                }, 
            "systemd_network" : {
                "networkmanager" : ("NetworkManager"),
                "connman" : ("connman"),
                "dhcpcd" : ("dhcpcd")
                }, 
            "pack_plymouth"      : ("plymouth"),
            },
        #"grub" : ("grub", "os-prober")
        "gentoo" : {
            # "pack_base" : ("base", "base-devel"),
            #"flashplugin", "icedtea-web", "jre7-openjdk
            "pack_common" : ("openssh", "nfs-utils", "zip", "unzip", "ntfs3g", "dosfstools", "nano"),  #, "unrar"),
            "pack_desktop" : ("xorg-server", "vlc", "x264", "noto", "gst-plugins-bad", "gst-plugins-base", "gst-plugins-good", "gst-plugins-ugly"),
            "pack_grub" : ("grub"),
            "pack_grub_efi" : ("grub", "os-prober", "efibootmgr"),
            #"pack_grub" : ("grub", "os-prober"),
            "pack_syslinux" : ("syslinux"),
            "pack_lang" : ("noto-fonts-cjk"),
            "pack_nav"  : ("firefox"),
            "pack_drv" : {
                "amdgpu"     : ("xf86-video-amdgpu"),   
                "radeon"     : ("xf86-video-ati"),      
                "intel"      : ("xf86-video-intel"),      
                "virtualbox" : ("virtualbox-guest-additions", "xf86-video-vboxvideo" ), #, "virtualbox-guest-modules-arch"),
                "nouveau"    : ("xf86-video-nouveau"),      
                "tous"       : ("xf86-video-amdgpu", "xf86-video-ati", "xf86-video-intel", "xf86-video-nouveau", "virtualbox-guest-additions", "xf86-video-vmware"), 
                "nvidia"     : ("nvidia nvidia-libgl nvidia-utils"),      
                "nvidia-340xx":("nvidia-340xx", "nvidia-340xx-libgl", "nvidia-340xx-utils"),      
                "nvidia-304xx":("nvidia-304xx", "nvidia-304xx-libgl", "nvidia-304xx-utils"),     
                }, 
            "pack_net" : {
                "networkmanager"       : ("networkmanager"),   
                "connman"       : ("connman"),      
                "dhcpcd"     : ("dhcpcd"),      
                "nfsroot"     : ("")
                }, 
            "pack_environnement" : {
                EN_ENVIRONNEMENT.PLASMA : ("plasma-desktop", "konsole", "kde-apps/dolphin", "kate", "gwenview", "okular", "strawberry", "ark", "kfind", "systemsettings"),
                EN_ENVIRONNEMENT.GNOME : (), 
                EN_ENVIRONNEMENT.MATE : ("mate-terminal", "caja", "pluma", "engrampa", "eom", "atril", "mate-control-center", "gvfs", "mate-session-manager", "mate-themes", "mate-panel"), 
                EN_ENVIRONNEMENT.CINNAMON : (), 
                EN_ENVIRONNEMENT.XFCE : (), 
                EN_ENVIRONNEMENT.LXDE : ("lxterminal", "pcmanfm", "leafpad", "gpicview", "gvfs", "gvfs-smb", "community/epdfview", "lxde-icon-theme", "lxrandr", "openbox", "lxappearance", "lxappearance-obconf", "lxde-common", "lxinput", "lxlauncher", "lxmenu-data", "lxpanel", "lxsession", "lxtask"), 
                EN_ENVIRONNEMENT.LXQT : ("lxqt-config", "lxqt-globalkeys", "lxqt-notificationd", "lxqt-openssh-askpass", "lxqt-panel", "lxqt-policykit", "lxqt-qtplugin", "lxqt-runner", "lxqt-session", "openbox", "pcmanfm-qt", "qterminal", "xcompmgr","breeze" , "breeze-icons"), 
                EN_ENVIRONNEMENT.ENLIGHTENMENT : (), 
                EN_ENVIRONNEMENT.FLUXBOX : (), 
                EN_ENVIRONNEMENT.DEEPIN : (), 
                EN_ENVIRONNEMENT.BUDGIE_DESKTOP : (), 
                EN_ENVIRONNEMENT.I3 : (), 
                }, 
            "pack_displaymanager" : {
                EN_DISPLAYMANAGER.SDDM : (EN_DISPLAYMANAGER.SDDM),
                EN_DISPLAYMANAGER.GDM : (EN_DISPLAYMANAGER.GDM),
                EN_DISPLAYMANAGER.LIGHTDM : (EN_DISPLAYMANAGER.LIGHTDM, EN_DISPLAYMANAGER.LIGHTDM + "-gtk-greeter", "accountsservice"),
                EN_DISPLAYMANAGER.LXDM : (EN_DISPLAYMANAGER.LXDM),
                EN_DISPLAYMANAGER.SLIM : (EN_DISPLAYMANAGER.SLIM),
                EN_DISPLAYMANAGER.XDM : (EN_DISPLAYMANAGER.XDM),
                EN_DISPLAYMANAGER.NODM : (EN_DISPLAYMANAGER.NODM)
                }, 
            "pack_cups"      : ("cups"),
            "pack_hplip"      : ("hplip"),
            "pack_bluez"      : ("bluez", "bluez-utils", "bluez-tools"),
            "pack_office"      : ("libreoffice-still"),
            "pack_mail"      : ("thunderbird"),
            "pack_mesa"      : ("mesa"),
            "pack_wifi" : ( "wpa_supplicant", "iw"), 
            "pack_tool_netm" : ("network-manager-applet"), 
            #"systemd_enable" : ("nfs-client.target", "sshd"),
            "systemd_enable" : ("nfs-client.target", "sshd"), 
            "systemd_displaymanager" : {
                EN_DISPLAYMANAGER.SDDM : (EN_DISPLAYMANAGER.SDDM),
                EN_DISPLAYMANAGER.GDM : (EN_DISPLAYMANAGER.GDM),
                EN_DISPLAYMANAGER.LIGHTDM : (EN_DISPLAYMANAGER.LIGHTDM),
                EN_DISPLAYMANAGER.LXDM : (EN_DISPLAYMANAGER.LXDM),
                EN_DISPLAYMANAGER.SLIM : (EN_DISPLAYMANAGER.SLIM),
                EN_DISPLAYMANAGER.XDM : (EN_DISPLAYMANAGER.XDM),
                EN_DISPLAYMANAGER.NODM : (EN_DISPLAYMANAGER.NODM)
                }, 
            "systemd_network" : {
                "networkmanager" : ("NetworkManager"),
                "connman" : ("connman"),
                "dhcpcd" : ("dhcpcd")
                }, 
            "pack_plymouth"      : ("plymouth"),
            "use" : {
                EN_ENVIRONNEMENT.PLASMA : ("plasma-desktop", "konsole", "kde-apps/dolphin", "kate", "gwenview", "okular", "strawberry", "ark", "kfind"),
                EN_ENVIRONNEMENT.GNOME : (),
                EN_ENVIRONNEMENT.MATE : ("mate-terminal", "caja", "pluma", "engrampa", "eom", "atril", "mate-control-center", "gvfs", "mate-session-manager", "mate-themes", "mate-panel"),
                EN_ENVIRONNEMENT.CINNAMON : (),
                EN_ENVIRONNEMENT.XFCE : (),
                EN_ENVIRONNEMENT.LXDE : ("lxterminal", "pcmanfm", "leafpad", "gpicview", "gvfs", "gvfs-smb", "community/epdfview", "lxde-icon-theme", "lxrandr", "openbox", "lxappearance", "lxappearance-obconf", "lxde-common", "lxinput", "lxlauncher", "lxmenu-data", "lxpanel", "lxsession", "lxtask"),
                EN_ENVIRONNEMENT.LXQT : ("lxqt-config", "lxqt-globalkeys", "lxqt-notificationd", "lxqt-openssh-askpass", "lxqt-panel", "lxqt-policykit", "lxqt-qtplugin", "lxqt-runner", "lxqt-session", "openbox", "pcmanfm-qt", "qterminal", "xcompmgr","breeze" , "breeze-icons"),
                EN_ENVIRONNEMENT.ENLIGHTENMENT : (),
                EN_ENVIRONNEMENT.FLUXBOX : (),
                EN_ENVIRONNEMENT.DEEPIN : (),
                EN_ENVIRONNEMENT.BUDGIE_DESKTOP : (),
                EN_ENVIRONNEMENT.I3 : (),
                },
            "package.use" : {
                EN_ENVIRONNEMENT.PLASMA : "-gtk -gnome",
                EN_ENVIRONNEMENT.GNOME : "-kde -qt5",
                # EN_ENVIRONNEMENT.MATE : ("mate-terminal", "caja", "pluma", "engrampa", "eom", "atril", "mate-control-center", "gvfs", "mate-session-manager", "mate-themes", "mate-panel"),
                # EN_ENVIRONNEMENT.CINNAMON : (),
                # EN_ENVIRONNEMENT.XFCE : (),
                # EN_ENVIRONNEMENT.LXDE : ("lxterminal", "pcmanfm", "leafpad", "gpicview", "gvfs", "gvfs-smb", "community/epdfview", "lxde-icon-theme", "lxrandr", "openbox", "lxappearance", "lxappearance-obconf", "lxde-common", "lxinput", "lxlauncher", "lxmenu-data", "lxpanel", "lxsession", "lxtask"),
                # EN_ENVIRONNEMENT.LXQT : ("lxqt-config", "lxqt-globalkeys", "lxqt-notificationd", "lxqt-openssh-askpass", "lxqt-panel", "lxqt-policykit", "lxqt-qtplugin", "lxqt-runner", "lxqt-session", "openbox", "pcmanfm-qt", "qterminal", "xcompmgr","breeze" , "breeze-icons"),
                # EN_ENVIRONNEMENT.ENLIGHTENMENT : (),
                # EN_ENVIRONNEMENT.FLUXBOX : (),
                # EN_ENVIRONNEMENT.DEEPIN : (),
                # EN_ENVIRONNEMENT.BUDGIE_DESKTOP : (),
                # EN_ENVIRONNEMENT.I3 : (),
                },
            }, 
        "debian" : {
            "pack_base" : ("base", "base-devel"),
            "pack_common" : ( "nfs-common", "openssh-client", "openssh-server", "zip", "unzip", "unrar-free", "ntfs-3g ", "dosfstools"),
            "pack_desktop" : ("xserver-xorg-core", "xserver-xorg-input-libinput", "vlc", "alsa-utils"),
            "pack_grub" : ("grub-pc"),
            "pack_grub_efi" : ("grub-efi", "os-prober", "efibootmgr"),
            "pack_syslinux" : ("syslinux"),
            "pack_lang" : ("noto-fonts-cjk"),
            "pack_nav"  : ("firefox-esr"),
            "pack_drv" : {
                "amdgpu"       : ("xserver-xorg-video-amdgpu"),   
                "radeon"       : ("xserver-xorg-video-ati"),      
                "intel"     : ("xserver-xorg-video-intel"),      
                "virtualbox"   : (),      
                "nouveau"   : ("xserver-xorg-video-nouveau"),      
                "tous"       : ("xserver-xorg-video-amdgpu", "xserver-xorg-video-ati", "xserver-xorg-video-intel", "xserver-xorg-video-nouveau", "xserver-xorg-video-vmware"), 
                "nvidia"        : ("nvidia nvidia-libgl nvidia-utils"),      
                "nvidia-340xx"     : ("nvidia-340xx", "nvidia-340xx-libgl", "nvidia-340xx-utils"),      
                "nvidia-304xx"     : ("nvidia-304xx", "nvidia-304xx-libgl", "nvidia-304xx-utils"),      
                }, 
            "pack_net" : {
                "networkmanager"       : ("network-manager"),   
                "connman"       : ("connman"),      
                "dhcpcd"     : ("dhcpcd"),
                "nfsroot"     : ("")
                },
            "pack_environnement" : {
                EN_ENVIRONNEMENT.PLASMA : ("plasma-workspace-wayland", "kde-full", "konsole", "dolphin", "kate", "gwenview", "okular", "strawberry", "ark", "kfind", "kcalc", "dvd+rw-tools cdrdao k3b"),
                EN_ENVIRONNEMENT.GNOME : (), 
                EN_ENVIRONNEMENT.MATE : ("mate-desktop-environment-core", "mate-menu"), 
                EN_ENVIRONNEMENT.CINNAMON : (), 
                EN_ENVIRONNEMENT.XFCE : (), 
                EN_ENVIRONNEMENT.LXDE : (), 
                EN_ENVIRONNEMENT.LXQT : ("lxqt-config", "lxqt-globalkeys", "lxqt-notificationd", "lxqt-openssh-askpass", "lxqt-panel", "lxqt-policykit", "lxqt-qtplugin", "lxqt-runner", "lxqt-session", "openbox", "pcmanfm-qt", "qterminal", "breeze-icon-theme"), 
                EN_ENVIRONNEMENT.ENLIGHTENMENT : (), 
                EN_ENVIRONNEMENT.FLUXBOX : (), 
                EN_ENVIRONNEMENT.DEEPIN : (), 
                EN_ENVIRONNEMENT.BUDGIE_DESKTOP : (), 
                EN_ENVIRONNEMENT.I3 : (), 
                }, 
            "pack_displaymanager" : {
                EN_DISPLAYMANAGER.SDDM : (EN_DISPLAYMANAGER.SDDM),
                EN_DISPLAYMANAGER.GDM : (EN_DISPLAYMANAGER.GDM),
                EN_DISPLAYMANAGER.LIGHTDM : (EN_DISPLAYMANAGER.LIGHTDM),
                EN_DISPLAYMANAGER.LXDM : (EN_DISPLAYMANAGER.LXDM),
                EN_DISPLAYMANAGER.SLIM : (EN_DISPLAYMANAGER.SLIM),
                EN_DISPLAYMANAGER.XDM : (EN_DISPLAYMANAGER.XDM),
                EN_DISPLAYMANAGER.NODM : (EN_DISPLAYMANAGER.NODM)
                }, 
            "pack_cups"      : ("cups"),
            "pack_hplip"      : ("hplip"),
            "pack_bluez"      : ("bluez", "bluez-tools"),
            "pack_office"      : ("libreoffice"),
            "pack_mail"      : ("thunderbird"),
            "pack_mesa"      : ("mesa"),
            "pack_wifi" : ( "wpa_supplicant", "iw"), 
            "pack_tool_netm" : ("network-manager-applet"), 
            "systemd_enable" : (),
            #"systemd_enable" : ("nfs-client.target", "sshd"), 
            "systemd_displaymanager" : {
                EN_DISPLAYMANAGER.SDDM : (),
                EN_DISPLAYMANAGER.GDM : (),
                EN_DISPLAYMANAGER.LIGHTDM : (),
                EN_DISPLAYMANAGER.LXDM : (),
                EN_DISPLAYMANAGER.SLIM : (),
                EN_DISPLAYMANAGER.XDM : (),
                EN_DISPLAYMANAGER.NODM : ()
                }, 
            "systemd_network" : {
                "networkmanager" : (""),
                "connman" : (""),
                "dhcpcd" : ("")
            },
            "pack_plymouth"      : ("plymouth"),
        }
    }
