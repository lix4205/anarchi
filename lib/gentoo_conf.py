# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# coding: utf-8
#import subprocess


import time

from multiprocessing.pool import ThreadPool
from lib.user_conf import user_conf
#from lib import exec_cmd
import datetime
import tarfile
from threading import Timer
import os
class gentoo_conf(user_conf) :
    PACK_MANAGER="emerge"
    PACK_MANAGER_OPTS=""
    PACK_MANAGER_SEARCH="-s"
    PACK_MANAGER_INSTALL=""  # 
    PACK_MANAGER_UPDATE="--sync"
    PACK_MANAGER_UPGRADE="-"
    PACK_MANAGER_NOCONFIRM=""
    #PACK_MANAGER_MULTILIB="\n#Multilib configuration\n[multilib]\nInclude = /etc/pacman.d/mirrorlist"
    #PACK_MANAGER_AUR="\n#AUR configuration\n[archlinuxfr]\nServer = http://repo.archlinux.fr/\$arch\nSigLevel = Never"
    
    BASE_PACKAGES = "base base-devel linux"
    ARCH_CMD = {
        # Main directories creation
        #"create_root" : "mkdir -m 0755 -p {}/var/{{cache/pacman/pkg,lib/pacman,log}} {}/{{dev,run,etc}}", 
        "create_root" : "mkdir -m 0755 -p \"{}/var/cache/distfiles\" \"{}/var/log\" \"{}/dev\" \"{}/run\" \"{}/etc\"",
        "create_tmp" : "mkdir -m 1777 -p \"{}/tmp\"", 
        "create_proc" : "mkdir -m 0555 -p \"{}/sys\" \"{}/proc\"", 

        # Make.conf
        #"make.conf_common" : "sed -i \"s/COMMON_FLAGS=\(\"\).*\(\"\)/COMMON_FLAGS=\\1-march=native -O2 -pipe\\2/g\" {}/etc/portage/make.conf",
        "make.conf_common" : "sed -i \"s/-O2 -pipe/-march=native -O2 -pipe/g\" {}/etc/portage/make.conf",
        "make.conf_opts" : "echo MAKEOPTS=\\\"-j8\\\" >> {}/etc/portage/make.conf",
        #"make.conf_common" : "sed -i s/COMMON_FLAGS=.*/COMMON_FLAGS=\\\"-march=native -O2 -pipe\\\"/g {}/etc/portage/make.conf",
        #"make.conf_opts" : "echo \"MAKEOPTS=\\\"-j5\\\"\" >> {}/etc/portage/make.conf",

        "mirror_select" : "mirrorselect -i -o >> {}/etc/portage/make.conf", 
        "conf_copy_resolv" : "cp --dereference /etc/resolv.conf {}/etc/",
        "conf_ebuild_mkdir" : "mkdir --parents {}/etc/portage/repos.conf",
        "conf_ebuild_cp" : "cp {}/usr/share/portage/config/repos.conf {}/etc/portage/repos.conf/gentoo.conf",
        
        "path_gentoo" : "source /etc/profile",
        
        "port_init" : "emerge-webrsync", 
        "profile_list" : "eselect profile list",
        "profile_select" : "eselect profile set {}",
        "video_card_select" : "echo VIDEO_CARDS=\\\"{}\\\" >> \"{}/etc/portage/make.conf\"",
        
        "merge_detect_config" : "emerge --noreplace app-portage/cpuid2cpuflags resolve-march-native",
        "cpuflags" : "! grep -q \"$(chroot {} cpuid2cpuflags)\" {}/etc/portage/package.use/00cpu-flags 2> /dev/null && echo \"*/* $(chroot {} cpuid2cpuflags)\" > \"{}/etc/portage/package.use/00cpu-flags\" || echo false",

        # "merge_resolve_native" : "emerge resolve-march-native",
        "resolve_native" : "! grep -q '$(chroot {} resolve-march-native)' {}/etc/portage/make.conf && sed -i \"s/COMMON_FLAGS=\\\"/COMMON_FLAGS=\\\"$(chroot {} resolve-march-native) /g\" \"{}/etc/portage/make.conf\" || echo false",

        "makeopts" : "echo \"MAKEOPTS=\"-j$(nproc)\" > \"{}/etc/portage/make.conf\"",

        "merge_world" : "emerge --verbose --update --noreplace --deep --newuse @world",
        "install_linux" : "emerge --oneshot --autounmask-write --noreplace sys-kernel/gentoo-sources sys-kernel/genkernel",
        "compil_linux" : "genkernel all",
        "select_linux" : "eselect kernel set {}",
        "compil_initramfs" : "genkernel --install initramfs",

        "license_accept_all" : "mkdir -p {}/etc/portage/package.license && echo \"*/* *\" >> {}/etc/portage/package.license/custom",

        # Timezone/Locales/keymap
        #"hostname" : "echo {} > {}/etc/hostname", 
        "timezone" : "echo \"{}\" > {}/etc/timezone", 
        "timezone_set" : "emerge --config sys-libs/timezone-data", 
        "localeset" : "echo \"{} UTF-8\" > \"{}/etc/locale.gen\"", # TODO : A verifier !!! 
        #"localeset" : "sed -i s/\#{}/{}/g /etc/locale.gen", # in chroot
        "localegen" : "locale-gen", # in chroot
        
        "env-update" : "env-update", 

        "locale" : "echo -e LANG=\"{}\nLC_COLLATE=\"C\"\" > \"{}/etc/env.d/02locale\"", 
        "keymap" : "echo KEYMAP={} > \"{}/etc/vconsole.conf\"",
        # "keymap" : "localectl set-keymap {}",
        "xkeyboard" :  "echo '" + user_conf.KEYBOARD_CONF +"' > \"{}/etc/X11/xorg.conf.d/00-keyboard.conf\"" ,

        # Fstab
        "genfstab" : "genfstab -U \"{}\" > \"{}/etc/fstab\"",

        # hostname/hosts
        "hostname" : "echo \"{}\" > \"{}/etc/hostname\"", 
        "hosts" : "echo -e \"" + user_conf.HOSTS_ENTRY +"\" > \"{}/etc/hosts\"",
       }
        
    # ARCH_CMD_PACMAN = {
    #     "update" : PACK_MANAGER + " -r {} " + PACK_MANAGER_UPDATE, #. format("{}/var/{{cache/pacman/pkg,lib/pacman,log}}", "{}/{{dev,run,etc}}"),
    #     "install_base" : PACK_MANAGER + " -r {} " + PACK_MANAGER_INSTALL + " --needed {}",
    #     "gnupg" : "cp -a /etc/pacman.d/gnupg {}/etc/pacman.d/",
    #     "mirrorlist" : "cp -a /etc/pacman.d/mirrorlist {}/etc/pacman.d/",
    #     "install_pack" : PACK_MANAGER + " -r {} " + PACK_MANAGER_INSTALL + " --needed {}",  }

    CMD_NFSROOT = [
        "sed -i \"s/MODULES=(/MODULES=({} /g\" /etc/mkinitcpio.conf",
        "sed s/nfsmount/mount.nfs4/ \"/usr/lib/initcpio/hooks/net\" > \"{}/usr/lib/initcpio/hooks/net_nfs4\"", 
        "echo \"tmpfs   /var/log        tmpfs     nodev,nosuid    0 0\" >> \"{}/etc/fstab\"" ,
        "echo -e \"# For Cups :\n#tmpfs   /var/spool/cups tmpfs     nodev,nosuid    0 0\" >> \"{}/etc/fstab\"",
        "sed -i \"s/BINARIES=(/BINARIES=(\\/usr\\/bin\\/mount.nfs4 /g\" /etc/mkinitcpio.conf" ,
        "sed -i \"s/ fsck//\" /etc/mkinitcpio.conf" ,
        "sed -i \"s/HOOKS=(/HOOKS=(net_nfs4 /g\" /etc/mkinitcpio.conf" ,
        "cp /usr/lib/initcpio/install/net{,_nfs4}", 
        
        "mv /var/log /var/_log",
        "mkdir /var/log", 
        "mkinitcpio -p linux" ]
    DEFAULT_USE = [ "bluetooth", "networkmanager", "cups" ]
    GENTOO_CONF = user_conf.SYSTEM_CONF.DEFAULT_CONF["gentoo"]
    # USE_VARIABLE = {
    #     "gnome" : "-kde -qt5",
    #     "plasma" : "-gtk -gnome"
    #     }
    
    
    def __init__(self):
        super().__init__() 
        
    def __init__(self, user_input, conf_user, local, graphic, reseau, mirrorlist = True, copykeyring = True):
        super().__init__(local, graphic, reseau, conf_user.GRUB_INSTALL)
        self.PATH_INSTALL = conf_user.PATH_INSTALL 
        self.USER_NAME = conf_user.USER_NAME
        self.USER_PASSWD= conf_user.USER_PASSWD
        self.ROOT_PASSWD= conf_user.ROOT_PASSWD
        self.SUDO = conf_user.SUDO
        self.hostname = conf_user.hostname
        self.DISTRIB = "gentoo"
        self.REAL_ROOT = self.PATH_INSTALL
        self.SUDO_SET = conf_user.SUDO_SET
        #self.PACK_MANAGER_OPTS = conf_user.PACK_MANAGER_OPTS
        
        self.RAM_INSTALL = conf_user.RAM_INSTALL
        self.QUIET = conf_user.QUIET
        self.TEST_MODE = conf_user.TEST_MODE
        self.INTERACTIVE = conf_user.INTERACTIVE
        self.PACKAGES += conf_user.PACKAGES
        
        if self.INTERACTIVE :
            self.PACK_MANAGER_NOCONFIRM == ""
        
        # Get classes to manage user IO
        self.INPUTS = user_input
        self.COPYMIRRORLIST = mirrorlist
        self.COPYKEYRING = copykeyring
        
        self.HOST_ARCH="x86_64"
        self.NEW_ARCH = ""
        self.MIRROR="http://mirror.f4st.host/archlinux/"
        
        self.PACK_MANAGER=gentoo_conf.PACK_MANAGER
        self.PACK_MANAGER_NOCONFIRM = gentoo_conf.PACK_MANAGER_NOCONFIRM
        self.PACK_MANAGER_SEARCH = self.PACK_MANAGER + " -s"
        self.PACK_MANAGER_INSTALL = self.PACK_MANAGER + " " + self.PACK_MANAGER_NOCONFIRM # 
        self.PACK_MANAGER_UPDATE = self.PACK_MANAGER + " " + self.PACK_MANAGER_NOCONFIRM + " -Sy"
        self.PACK_MANAGER_UPGRADE = self.PACK_MANAGER + " " + self.PACK_MANAGER_NOCONFIRM + " -Syu"
        self.PACK_MANAGER_CACHE_DEFAULT = "/var/cache/distfiles"
        self.PACK_MANAGER_CACHE_PKG = conf_user.PACK_MANAGER_CACHE_PKG # self.PACK_MANAGER_CACHE_DEFAULT 

        self.prerequis = "genfstab"
        self.PACMAN_CONFIG_FILE = "/etc/pacman.conf"
        
        self.file_zoneinfo = "/usr/share/zoneinfo/zone.tab"
        self.file_kbdmap = "/usr/share/kbd/keymaps"
        self.file_kbdmapext = "map.gz"
        #self.file_xkbdmap = ""
        self.file_locale = "/etc/locale.gen"

        now = datetime.datetime.now()
        NAME_ARCHIVE = "gentoo-bootstrap"
        #https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/20200701T214503Z/stage3-amd64-20200701T214503Z.tar.xz
        #self.ARCH_IMG_URL="http://mirrors.kernel.org/archlinux/iso/latest/archlinux-bootstrap-" +now.strftime("%Y.%m.01")+ "-" + self.HOST_ARCH + ".tar.gz"
        self.ARCH_IMG_URL="https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/20200701T214503Z/stage3-amd64-20200701T214503Z.tar.xz"
        self.ARCH_IMG_URL="https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/current-stage3-amd64-desktop-systemd/stage3-amd64-desktop-systemd-20240505T170430Z.tar.xz"
        self.ARCH_IMG_URL = "https://gentoo.mirrors.ovh.net/gentoo-distfiles/releases/amd64/autobuilds/current-stage3-amd64-desktop-systemd/stage3-amd64-desktop-systemd-20240616T153408Z.tar.xz"


        #self.ARCH_IMG_URL="http://calis-asso.org/" + NAME_ARCHIVE + ".tar.xz"
        self.DOWNLOAD_OK = True
        self.NO_PACMAN = False
        self.ROOT_BOOTSTRAP = self.DIR_WORK + "/root.x86_64"
        self.ROOT_DIR_BOOTSTRAP="/install.arch"
        self.ARCH_IMG_FILE = self.PATH_INSTALL + "/" + NAME_ARCHIVE + ".tar.xz"
        self.LOG_EXE="/tmp/anarchi.log"
        self.FILE_COMMANDS="/tmp/anarchi_command"
        self.SYSTEMD_ENABLE = self.GetPackages("systemd_enable")
        
        self.REAL_ROOT = self.PATH_INSTALL
        
        self.AddPackages()
        self.USE_DISTCC = False
        self.PROFILE = 24
        self.VIDEO_DRV = ""
        self.USE_VARIABLE = ""
        self.running = user_conf.chroot.chroot_init(self.PATH_INSTALL, self.TEST_MODE, self.QUIET, True)
        
       # user_input.msg_n("Checking {}", "32", "32", "environnement")
        # if not self.check_command(self.prerequis) :
        if not self.exist_install(user_input, self.prerequis, "arch-install-scripts") :
            self.INPUTS.die("Pas de {}, pas de {} !", self.prerequis, "chocolat")

        self.INPUTS.LOADING = False
        
        self.OVERRIDE_PATH = gentoo_conf.ARCH_CMD["path_gentoo"]
        if not os.path.exists(os.path.join(self.PATH_INSTALL, "usr/bin/emerge")) :
            if not os.path.exists(self.ARCH_IMG_FILE) :
                if self.INPUTS.rid_continue("Télécharger {} ?", "Gentoo") :
                    #self.NO_PACMAN = True
                    #print("Downloading ... " + self.ARCH_IMG_URL)
                    ThreadPool(1).imap_unordered(self.download_img, [(self.ARCH_IMG_FILE ,self.ARCH_IMG_URL)])
                    self.DOWNLOAD_OK = False
                    #start = time()

                else :
                    self.INPUTS.die("Pas d'{}, pas de {} !", "emerge", "chocolat")
            else :
                self.DOWNLOAD_OK = True
        else :
            self.DOWNLOAD_OK = True
            #     self.WaitForExtract(True)
        # print(user_conf.chroot.run_command.DIR_DONE)
        # dir_done = os.path.join(self.PATH_INSTALL, "var", user_conf.chroot.run_command.DIR_DONE)
        user_conf.chroot.run_command.DIR_DONE = os.path.join(self.PATH_INSTALL, "var") + user_conf.chroot.run_command.DIR_DONE
        # print(user_conf.chroot.run_command.DIR_DONE)
        # import sys
        # sys.exit(0)
            
    def download_img(self, url) :
        try:
            path, url = url
            #self.running.add("mkdir -p \"" + self.ROOT_BOOTSTRAP + "\"")
            # print(path)
            if not os.path.exists(path) :
                self.running.add("wget " + url + " -O " + path + " 2> /dev/null")
                # print("wget " + url + " -O " + path + " 2> /dev/null")
                if not self.running.run_all_once("anarchi_stage3_dl") == 0 :
                    self.DOWNLOAD_OK = True
                    raise Exception("Unable to download {}".format(self.ARCH_IMG_URL))
                    # return False
            self.DOWNLOAD_OK = True
            #if not self.TEST_MODE :
                #os.rename(path + ".part", path)
            return self.extract_tar()
            
            # self.running.add("cd " + self.PATH_INSTALL + " && tar xpf  " + path + " --xattrs-include='*.*' --numeric-owner")
            # if not self.running.run_all_once("anarchi_stage3_extract") :
            #     return False
        except Exception as ex:
            print("==> ERROR : Unable to retrieve {}\n  -> {}".format(url, ex))
            return False
        
    def extract_tar(self) :
        try :
            self.running.add("cd " + self.PATH_INSTALL + " && tar xpf  " + self.ARCH_IMG_FILE + " --xattrs-include='*.*' --numeric-owner")
            if not self.running.run_all_once("anarchi_stage3_extract") == 0 :
                return False
            # tar = tarfile.open(self.ARCH_IMG_FILE, "r:xz")
            # tar.extractall(path=self.DIR_WORK)
            # tar.close()
            return True
        except Exception as ex :
            print()
            self.INPUTS.die("Extract failed !\n{}".format(ex))
            return False
            
    def WaitForExtract(self, run_loading = True) :
        try :
            # print(os.path.join(self.PATH_INSTALL, "usr/bin/emerge"))
            if not os.path.exists(os.path.join(self.PATH_INSTALL, "usr/bin/emerge")) :
                self.INPUTS.msg_nn("Please wait while extracting {} ...", "gentoo stage 3")
                if run_loading :
                    self.INPUTS.loading()
                #ThreadPool(2).imap_unordered(self.INPUTS.loading, [( user_input.msg_nn("Extracting {}...", self.ARCH_IMG_FILE) ) ])
                self.INPUTS.LOADING = True
                self.extract_tar()
                self.INPUTS.LOADING = False       
                print("\bdone")
        except Exception as ex :
            print(ex)
            if os.path.exists(self.ROOT_BOOTSTRAP) :
                os.rmdir(self.ROOT_BOOTSTRAP)

    def SetAdditionnalPackage(self, addbluetooth, addhplip, addcups, addOffice, addMail ) :
        super().SetAdditionnalPackage(addbluetooth, addhplip, addcups, addOffice, addMail)
        var_use = ""
        print("Configuring USE")
        if not addbluetooth :
            # print("grep -q \"\-bluetooth\" {}/etc/portage/make.conf".format(self.PATH_INSTALL))
            if user_conf.chroot.run_command.TEST_MODE or self.running.check_call("grep -q \"\-bluetooth\" {}/etc/portage/make.conf".format(self.PATH_INSTALL)) != 0 :
                var_use += "-bluetooth "

        if not addcups :
            if user_conf.chroot.run_command.TEST_MODE or self.running.check_call("grep -q \"\-cups\" {}/etc/portage/make.conf".format(self.PATH_INSTALL)) != 0 :
                var_use += "-cups "

        env_de = self.GetEnvironement()
        if env_de in gentoo_conf.GENTOO_CONF["package.use"] :
            # print("grep -q \"\{}\" {}/etc/portage/make.conf".format(gentoo_conf.GENTOO_CONF["package.use"][env_de], self.PATH_INSTALL))
            if user_conf.chroot.run_command.TEST_MODE or self.running.check_call("grep -q \"\{}\" {}/etc/portage/make.conf".format(gentoo_conf.GENTOO_CONF["package.use"][env_de], self.PATH_INSTALL)) != 0 :
                var_use += gentoo_conf.GENTOO_CONF["package.use"][env_de] + " "

        # echo USE variable in /etc/portage/make.conf
        if user_conf.chroot.run_command.TEST_MODE or self.running.check_call("grep -q \"USE=\" {}/etc/portage/make.conf".format(self.PATH_INSTALL)) != 0 :
            # self.running.add("echo \"USE=\\\"\${USE} {}\\\"\" >> {}/etc/portage/make.conf".format(var_use[0:len(var_use) - 1], self.PATH_INSTALL))
            self.running.add("echo \"USE=\\\"{}\\\"\" >> {}/etc/portage/make.conf".format(var_use[0:len(var_use) - 1], self.PATH_INSTALL))
        else :
            self.running.add("sed -i \"s/USE=\\\"/USE=\\\"{}/\" {}/etc/portage/make.conf".format(var_use[0:len(var_use) - 1], self.PATH_INSTALL))

        if not self.running.run_all_once("anarchi_portage_use") == 0 :
            self.INPUTS.die("Error USE config !")


    def next_cmd(self) :
        return " --gentoo" + super().next_cmd()
    
    def SetUse(self, key) :
        if len(user_conf.system_conf.DistConf.DEFAULT_CONF["package.use"][key]) > 0 :
            cmd = "echo \"# " + key
            for p in user_conf.system_conf.DistConf.DEFAULT_CONF["package.use"][key] :
                cmd += p + "\n"

            cmd += "\" > \"{}/etc/portage/package.use/{}\"".format(self.PATH_INSTALL, key)
            print(cmd)


    def install(self) :
        """ Gentoo installation """
        # if self.INPUTS.rid_continue("Use {}", "distcc ?" ) :
        #     self.USE_DISTCC = True

        if not self.running.Init() :
            self.INPUTS.die("Error chroot config !")

        if not self.run_install(self.anarchi_pre_install,
                                        ("Preparing {} install", "gentoo"),
                                        ("Ready to install {} !", "gentoo"),
                                        ("Error occured while preparing {} installation !", "gentoo")) :
            sys.exit(1)
        
        
        user_conf.install(self)
                    
    def anarchi_pre_install(self) :
        self.running.add(gentoo_conf.ARCH_CMD["make.conf_common"].format(self.PATH_INSTALL))
        self.running.add(gentoo_conf.ARCH_CMD["make.conf_opts"].format(self.PATH_INSTALL))
        
        self.running.add(gentoo_conf.ARCH_CMD["conf_ebuild_mkdir"].format(self.PATH_INSTALL))
        self.running.add(gentoo_conf.ARCH_CMD["conf_ebuild_cp"].format(self.PATH_INSTALL, self.PATH_INSTALL))
        self.running.add(gentoo_conf.ARCH_CMD["conf_copy_resolv"].format(self.PATH_INSTALL))

        cmd_port_init = user_conf.USER_CMD_CHROOT.format("/bin/bash", self.OVERRIDE_PATH + "\n" + gentoo_conf.ARCH_CMD["port_init"])
        cmd_cpu_flags = gentoo_conf.ARCH_CMD["cpuflags"]
        cmd_resolve_native = gentoo_conf.ARCH_CMD["resolve_native"]

        self.running.add_chroot(cmd_port_init)
        if not self.running.run_all_once("anarchi_portage_init") == 0 :
            return False


        # print(cmd_port_init)
        cmd_profile_select = user_conf.USER_CMD_CHROOT.format("/bin/bash", self.OVERRIDE_PATH + "\n" + gentoo_conf.ARCH_CMD["profile_select"].format(self.PROFILE))
        # print(cmd_profile_select)
        # self.running.add_chroot(cmd_cpu_flags)
        # print(cmd_cpu_flags)

        # self.running.add_chroot(user_conf.USER_CMD_CHROOT.format("/bin/bash", self.OVERRIDE_PATH + "\n" + gentoo_conf.ARCH_CMD["merge_resolve_native"]))
        # self.running.add_chroot("echo 1")
        # self.running.add_chroot(cmd_resolve_native.format(self.PATH_INSTALL, self.PATH_INSTALL, self.PATH_INSTALL, self.PATH_INSTALL))



        #
        # if self.USE_DISTCC :
        #     self.running.add("echo -e \"Please configure distcc !\n\"")
        #     self.running.add_chroot(user_conf.USER_CMD_CHROOT.format("/bin/bash", self.OVERRIDE_PATH + "\nemerge --noreplace --autounmask-write distcc"))
        #     # print("Please configure distcc !")
        #     # self.running.add_chroot("/bin/bash")
        #     user_conf.chroot.run_command.run("chroot " + self.PATH_INSTALL + " ")

        if not user_conf.chroot.run_command.TEST_MODE :
            self.anarchi_select_profile()
        cmd_profile_select = user_conf.USER_CMD_CHROOT.format("/bin/bash", self.OVERRIDE_PATH + "\n" + gentoo_conf.ARCH_CMD["profile_select"].format(self.PROFILE))
        self.running.add_chroot(cmd_profile_select)
        if not self.running.run_all_once("anarchi_select_profile") == 0 :
            return False


        # self.running.add_chroot(gentoo_conf.ARCH_CMD["merge_detect_config"])
        # self.running.add(cmd_cpu_flags.format(self.PATH_INSTALL, self.PATH_INSTALL, self.PATH_INSTALL, self.PATH_INSTALL))
        # self.running.add(cmd_resolve_native.format(self.PATH_INSTALL, self.PATH_INSTALL, self.PATH_INSTALL, self.PATH_INSTALL))

        self.anarchi_set_graphic_driver()
        self.anarchi_set_use_variable()


        # sys.exit(0)
        return self.running.run_all_once(self.anarchi_pre_install.__name__) == 0

    def anarchi_set_use_variable(self) :
        """ Set use variable in make.conf """
        print("TODO : Personnal USE in make.conf")

    def anarchi_set_license_use(self) :
        """ Accept all licenses """
        if not os.path.exists("{}/etc/portage/package.license".format(self.PATH_INSTALL)) :
            self.running.add(gentoo_conf.ARCH_CMD["license_accept_all"].format(self.PATH_INSTALL, self.PATH_INSTALL))

    def anarchi_set_graphic_driver(self) :
        """ Set video driver in make.conf """
        vid_card = 1
        if not user_conf.chroot.run_command.TEST_MODE :
            vid_card = self.running.check_call("grep -q VIDEO_CARD " + self.PATH_INSTALL + "/etc/portage/make.conf")
        driver_card = self.GetVidDriver()
        if int(vid_card) != 0 :
            user_conf.chroot.run_command.run(gentoo_conf.ARCH_CMD["video_card_select"].format(driver_card, self.PATH_INSTALL))
        # "echo VIDEO_CARD=\\\"{}\\\" >> ".format(driver_card) + self.PATH_INSTALL + "/etc/portage/make.conf")
            # print(driver_card)


    def anarchi_select_profile(self) :
        """ Profile to use """
        # Get available profiles
        all_profiles = self.running.check_output("chroot " + self.PATH_INSTALL + " " + gentoo_conf.ARCH_CMD["profile_list"]).decode().split("\n")

        # Get current profile
        for p in all_profiles :
            if "*" in p :
                profile = p
                break
        # Ask for which profile to use
        if not self.INPUTS.rid_continue("Use profile {} ?", profile) :
            # Show and ask for profile to use
            p = self.INPUTS.rid_menu("Select profile", *all_profiles[1:len(all_profiles) - 1])
            self.PROFILE = p

    def anarchi_base(self) :
        """ Install base system """
        cmd_update = user_conf.USER_CMD_CHROOT.format("/bin/bash", self.OVERRIDE_PATH + "\n" + gentoo_conf.ARCH_CMD["merge_world"])

        self.running.add(gentoo_conf.ARCH_CMD["localeset"].format(self.GetLocale(), self.PATH_INSTALL))
        self.running.add_chroot(cmd_update)

        return self.running.run_all_once(self.anarchi_base.__name__) == 0
        
    def anarchi_conf(self) :
        try :
            res_cmd = 0
            cmd_envupdate = user_conf.USER_CMD_CHROOT.format("/bin/bash", self.OVERRIDE_PATH + "\n" + gentoo_conf.ARCH_CMD["env-update"])
            timezone_set = user_conf.USER_CMD_CHROOT.format("/bin/bash", self.OVERRIDE_PATH + "\n" + gentoo_conf.ARCH_CMD["timezone_set"])
            localegen = user_conf.USER_CMD_CHROOT.format("/bin/bash", self.OVERRIDE_PATH + "\n" + gentoo_conf.ARCH_CMD["localegen"])
            if not os.path.exists(user_conf.chroot.chroot_init.DIR_DONE) :
                os.mkdir(user_conf.chroot.chroot_init.DIR_DONE)
            # TODO : Verifier si ce test est utile /!\
            if not os.path.exists(user_conf.chroot.chroot_init.DIR_DONE + "/" + self.anarchi_conf.__name__) :
                self.running.add(gentoo_conf.ARCH_CMD["hostname"].format(self.hostname, self.PATH_INSTALL)) 
                self.running.add(gentoo_conf.ARCH_CMD["hosts"].format(self.hostname, self.hostname, self.PATH_INSTALL))
                
                self.SetLang()
                if self.GetNetworkConfig() != "nfsroot" :
                    self.running.add(gentoo_conf.ARCH_CMD["genfstab"].format(self.PATH_INSTALL, self.PATH_INSTALL))

                # Création du lien vers la bonne timezone dans : /usr/share/zoneinfo/
                self.running.add_chroot(timezone_set)
                # Generation de la locale avec locale-gen
                self.running.add_chroot(localegen)
                
                self.running.add_chroot(cmd_envupdate)

                return self.running.run_all_once( self.anarchi_conf.__name__ ) == 0
            else :
                return True
        except Exception as e :
            self.INPUTS.error("Error come from {}"".\n" + str(e), self.__name__)
        #finally :
            #return res_cmd == 0
    
    def SetLang(self) :
        try :
            # Affecte le 
            self.running.add(gentoo_conf.ARCH_CMD["localeset"].format(self.GetLocale(), self.PATH_INSTALL))
            # Modification de la locale du systeme dans le fichier /etc/locale.conf
            self.running.add(gentoo_conf.ARCH_CMD["locale"].format(self.GetLocale(), self.PATH_INSTALL))
            # Création du lien vers la bonne timezone dans : /usr/share/zoneinfo/
            self.running.add(gentoo_conf.ARCH_CMD["timezone"].format(self.GetTimeZone(), self.PATH_INSTALL))
            # Modification du codage clavier pour la console
            self.running.add(gentoo_conf.ARCH_CMD["keymap"].format(self.GetKeymap(), self.PATH_INSTALL))
        #return res_cmd 
            
        except Exception as e :
            self.INPUTS.error("Error come from {}"".\n" + str(e), self.__name__)
        #finally :
            #return res_cmd 
    def anarchi_kernel_compil(self) :
        # RACINE = self.PATH_INSTALL
        install_linux = user_conf.USER_CMD_CHROOT.format("/bin/bash", self.OVERRIDE_PATH + "\n" + gentoo_conf.ARCH_CMD["install_linux"])
        select_linux = user_conf.USER_CMD_CHROOT.format("/bin/bash", self.OVERRIDE_PATH + "\n" + gentoo_conf.ARCH_CMD["select_linux"].format(1))
        compil_linux = user_conf.USER_CMD_CHROOT.format("/bin/bash", self.OVERRIDE_PATH + "\n" + gentoo_conf.ARCH_CMD["compil_linux"])

        """ Accept all licenses """
        if not os.path.exists("{}/etc/portage/package.license".format(self.PATH_INSTALL)) :
            self.running.add(gentoo_conf.ARCH_CMD["license_accept_all"].format(self.PATH_INSTALL, self.PATH_INSTALL))

        # genkernel & gentoo sources (linux kernel) install
        self.running.add_chroot(install_linux)
        # Select kernel
        self.running.add_chroot(select_linux)
        # Gentoo kernel compilation
        self.running.add_chroot(compil_linux)
        if not self.running.run_all_once(self.anarchi_kernel_compil.__name__) == 0 :
            return False
        return True


    def anarchi_packages(self) :
        """
        Install packages
        Installation will automatically fail if some packages are masked,
        autounmask option will add new file if requested
        In this case, a retry is done...
        """
        if self.SUDO :
            self.PACKAGES += " sudo"

        install_pack = user_conf.USER_CMD_CHROOT.format("/bin/bash", self.OVERRIDE_PATH + "\nemerge --oneshot --noreplace --autounmask-write {}".format(self.PACKAGES_AUTO.strip()  + " " + self.PACKAGES.strip()))

        # Packages installation (1st attempt)
        self.running.add_chroot(install_pack)

        res_pak = self.running.run_all_once(self.anarchi_packages.__name__)

        if res_pak > 0 :
            # If previous installation packages failed, and a package.use file has been created, then we will retry...
            if not os.path.exists("{}/etc/portage/package.use/._cfg0000_zz-autounmask".format(self.PATH_INSTALL)) :
                # Package installation seems to have failed, so we exit
                return False

            # Move ._cfg0000_zz-autounmask to new env
            self.running.add("mv {}/etc/portage/package.use/._cfg0000_zz-autounmask {}/etc/portage/package.use/{}".format(self.PATH_INSTALL, self.PATH_INSTALL, self.GetEnvironement()))
            self.running.add_chroot(install_pack)
            res_pak = self.running.run_all_once(self.anarchi_packages.__name__)

        if res_pak > 0 :
            return res_pak == 0
        
        # Creation du fichier /etc/X11/xorg.conf.d/00-keyboard.conf
        if self.graphic.DESKTOP_ENV_IDX > -2 :
            CMD_CREATE = "mkdir -p " + self.PATH_INSTALL + "/etc/X11/xorg.conf.d/\n" + gentoo_conf.ARCH_CMD["xkeyboard"].format(self.GetXKbdmap().split("_")[0], self.PATH_INSTALL)
            
            res_cmd = user_conf.chroot.run_command.run(user_conf.USER_CMD_CHROOT.format("bash", CMD_CREATE))
        return res_cmd == 0
            
    def anarchi_grub(self, override_path_install = True) :
        cmd_grub = user_conf.GRUB_CONFIG.format(" loglevel=3", self.PATH_INSTALL)
        self.running.add(cmd_grub)

        return super().anarchi_grub(override_path_install)



    def anarchi_nfs_root(self) :
        gentoo_conf.CMD_NFSROOT[0] = gentoo_conf.CMD_NFSROOT[0].format(self.LIST_MODULES)
        gentoo_conf.CMD_NFSROOT[1] = gentoo_conf.CMD_NFSROOT[1].format(self.PATH_INSTALL)
        gentoo_conf.CMD_NFSROOT[2] = gentoo_conf.CMD_NFSROOT[2].format(self.PATH_INSTALL)
        gentoo_conf.CMD_NFSROOT[3] = gentoo_conf.CMD_NFSROOT[3].format(self.PATH_INSTALL)

        return user_conf.anarchi_nfs_root(self, gentoo_conf.CMD_NFSROOT)
  
    def anarchi_post_install(self) :
        #self.anarchi_gpg_init_root(self.PATH_INSTALL, "anarchi_gpg_init_root")
        return True
