#/bin/bash

# Files to archive
TO_ARCHIVE=( "custom.d" "files" "resources" "web" "bkp.sh" "dist_conf.py" "run_install.sh" "lib" )
# Archive path (current directory)
ARCHIVE_PATH="$(dirname "$(realpath $0)")"
# Archive filename
ARCHIVE_NAME="archives/$(date '+%Y%m%d')-$(basename "$ARCHIVE_PATH").tar.gz"

cd "$ARCHIVE_PATH"
if tar cf $ARCHIVE_NAME ${TO_ARCHIVE[*]}; then
    echo "$(basename "$ARCHIVE_NAME") created in $ARCHIVE_PATH/archives"
else
    echo "Unable to create $(basename "$ARCHIVE_NAME") in $ARCHIVE_PATH/archives"
fi
