# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# This script is executed at the end of the installation
# Don't forget you are chrooted in the fresh install while it run
# Put your stuff at the end of this file..
# 
#
# Some variables you can use :
# 	Username : NAME_USER=
# 	Les services systemd SYSTD_SOFT=
# 	Le disque sur lequel grub sera installé GRUB_DISK=
# 	L'interface reseau choisie NFSROOT=
# 	L'environnement de bureau DE=
# 	Le display manager DM=
# 	Le codage clavier X11 $X11_KEYMAP=
#	debian distribution installation DEBIAN_INSTALL=
# 	Cache des paquets déjà téléchargé CACHE_PKG=

# BEGIN USER CONFIG
# Par defaut :
# - Ajoute un fond d'ecran pour lightdm
# - Installe les paquets de yaourt (DECONSEILLÉ)
# - Configure xinitrc, pour slim, et nodm
# - Lance le fichier custom.d/$NAME_USER

# Vous pouvez decommentez certaines parties et les modifier à votre gré :
# - Monter un partage nfs et l'inscrire dans /etc/fstab ***
# - *** IMPORTANT ! Decommentez aussi la ligne concernant le demontage en fin de fichier 
# - Ajouter un fichier configurant le clavier pour tuer le serveur X avec ctrl+alt+backspace 
# - Copier des fichiers de configurations particuliers depuis le serveur NFS
# - Installer des logiciels selon l'environnement de bureau installé


ARCH_PACKAGES="$( [[ "$ARCH" == "x64" ]] && echo "x86_64" || echo "$ARCH" )"

# random background LIGHTDM
# if [[ $DM == "lightdm" ]]; then
# 	IMG_BG=$(find "/tmp/files/imgs/" -maxdepth 1 -type f | shuf | head -n 1 )
# 	EXT_BG_NAME=$( echo "$IMG_BG" | sed "s/.*\.//")
# 	BG_NAME="bg.$EXT_BG_NAME"
# 	cp "$IMG_BG" /usr/share/pixmaps/$BG_NAME
# 	sed -i "s/.*background=.*/background=\/usr\/share\/pixmaps\/$BG_NAME/" "/etc/lightdm/lightdm-gtk-greeter.conf"                
# fi
# # END

LOG_EXE="/tmp/anarchi.log"

function search_pack() {
	for f2e in "${@}"; do 
		if command -v $f2e >> /dev/null; then
			echo "$f2e"
			break
		fi
	done
}


function install_pack() {
	# Install packages
	if [[ ${#@} -gt 0 ]]; then
		_errors=0
		for _pack in "${@}"; do
			echo -n " -> Installation de $_pack"
			if ! $pacman ${packoptions[$pacman]} ${packinstall[$pacman]} "$_pack" >> $LOG_EXE 2>&1; then
				echo " ... failed !";
				_errors=1
				continue
			fi
			echo " ... ok"
		done

		(( _errors )) && echo "==> Consultez $LOG_EXE  pour plus de détail"
	fi
}

function set_kde_config() {
	# Set dark mode for kde,
	# Disable splash screen
	# yakuake config (w&h at 100%)
	_wallpaper=""
	if [[ ! -z "$2" ]]; then
		_wallpaper="plasma-apply-wallpaperimage \"$2\""
	fi
    su $1 <<EOF
[[ ! -e ~/Bureau ]] && mkdir -p ~/Bureau
echo '
#!/bin/bash

plasma-apply-lookandfeel -a org.kde.breezedark.desktop
plasma-apply-desktoptheme breeze-dark
$_wallpaper

rm ~/.config/autostart/auto-config.desktop
rm ~/Bureau/init_kde.sh

' > ~/Bureau/init_kde.sh

[[ ! -d ~/.config/autostart ]] && mkdir -p ~/.config/autostart

echo '[Desktop Entry]
Comment[fr_FR]=
Comment=
Exec=/usr/bin/bash /home/dux/Bureau/init_kde.sh
GenericName[fr_FR]=
GenericName=
Icon=
MimeType=
Name[fr_FR]=Samples
Name=Samples
Path=/home/dux/Bureau/
StartupNotify=true
Terminal=true
TerminalOptions=\\s
Type=Application
X-KDE-SubstituteUID=false
X-KDE-Username=' > ~/.config/autostart/auto-config.desktop


echo '[KSplash]
Engine=none
Theme=None' > ~/.config/ksplashrc

echo '[Window]
DynamicTabTitles=true
Height=100
KeepAbove=false
Width=100' > ~/.config/yakuakerc

cp /usr/share/applications/org.kde.yakuake.desktop ~/.config/autostart/
EOF
}

[[ -z $DEPENDENCIES ]] && DEPENDENCIES=( )

declare -A packinstall=(
	[pacman]="-Sy"
	[apt]="install"
	[yum]="install"
	[emerge]=""
)
	
declare -A packoptions=(
	[pacman]="--noconfirm --needed"
	[apt]="-y --no-install-recommends"
	[yum]=""
	[emerge]=""
)

declare -A packcache=(
	[pacman]="--cachedir"
# 	[apt]="-Y"
# 	[yum]=""
	[emerge]=""
)

packmanager=( "pacman" "apt" "yum" "emerge" )

cmd_packmanager=$(search_pack "${packmanager[@]}")

pacman="$cmd_packmanager"

# # Add cache package to command line
# [[ ! -z $CACHE_PKG ]] && packoptions[$pacman]+=" ${packcache[$pacman]} $CACHE_PKG"
# yaourt="yaourt --arch $ARCH_PACKAGES"
# echo $pacman ${packoptions[$pacman]} ${packinstall[$pacman]}
# exit 0


# BEGIN Serveur NFS 
# Va inscrire dans fstab la ligne correspondant au montage permanent du partage NFS
# avec x-systemd.automount,x-systemd.device-timeout=1s
# I use this directory to mount my NFS server...
DIR_SRV=/media/srv
# ...with this IP...
IP_SRV="192.168.0.11"

# BEGIN demontage du serveur
if [[ "$1" == "end" ]]; then
    until ! mountpoint -q $DIR_SRV; do        
            umount $DIR_SRV >> /dev/null 2>&1
            sleep 0.5
    done
    exit 0
fi
# END 
# Write in fstab
! cat /etc/fstab | grep -q $IP_SRV && 
echo -e "# $IP_SRV:/ on $DIR_SRV\n$IP_SRV:/	$DIR_SRV	nfs    users,noauto   0 0" >> /etc/fstab
# echo -e "# $IP_SRV:/ on $DIR_SRV\n$IP_SRV:/	$DIR_SRV	nfs    x-systemd.automount,x-systemd.device-timeout=1s   0 0" >> /etc/fstab
# Creation du repertoire
[[ ! -e /media/srv ]] && 
mkdir -p /media/{srv,tmp} && 
echo "==> Creating directories /media/{srv,tmp}"
# # Et on monte pour les configurations
# echo "==> Wait while mounting $DIR_SRV" &&
# mount $DIR_SRV
# END

# # BEGIN  install packages in files/de/yaourt.conf via AUR. 
# # files/de/yaourt.conf is empty by default 
# if [ "$LIST_YAOURT" != "" ]; then
# 	rep=
# 	while [[ "$rep" == "" ]]; do
# 		echo -en "\033[01;37m  -> Install AUR packages Y/n ?\033[00m ( $LIST_YAOURT ) "
# 		read -n 1 rep
# 		case "$rep" in 
# 			o|O|y|Y)
# 				echo "$LIST_YAOURT install" 
# 				su $NAME_USER -c "yaourt --arch $ARCH -Sy --noconfirm $LIST_YAOURT"
# 				break				
# 			;;
# 			n|N) break;;
# 			*) rep= ;;
# 		
# 		esac
# 	done
# fi

# BEGIN X11 KEYBOARD CONFIG ( Ctrl + Alt + backspace kill xserver)
# It's for a french keyboard ! Remove latin9 !!!
echo -e "Section \"InputClass\"\n\tIdentifier \"system-keyboard\"\n\tMatchIsKeyboard \"on\"\n\tOption \"XkbLayout\" \"${X11_KEYMAP}\"\n\tOption \"XkbModel\" \"pc105\"\n\tOption \"XkbVariant\" \"latin9\"\n\tOption \"XkbOptions\" \"terminate:ctrl_alt_bksp\"\nEndSection" > /etc/X11/xorg.conf.d/00-keyboard.conf
# END

[[ $? -eq 0 ]] && echo "==> La configuration pour $NAME_USER à été créée !"

# BEGIN configurations by the desktop-environnement
# Ici quelques commandes pour installer des terminal type quake
case $DE in
	plasma)
		DEPENDENCIES+=( "yakuake" )
		set_kde_config $NAME_USER
	;;
	# Cinnamon has tilda as terminal
	mate|xfce|gnome) 
		DEPENDENCIES+=( "tilda" )
	;;
	lxqt) : ;;
	fluxbox)
		su $NAME_USER -c "echo \"exec startfluxbox\" > /home/$NAME_USER/.xinitrc" 
  ;;
esac

# packages installation
install_pack "${DEPENDENCIES[@]}"

# END

# Some aliases
su $NAME_USER -c "echo -e \"# Some aliases\\nalias ll=\\\"ls -l\\\"\nalias la=\\\"ls -a\\\" \" >> /home/$NAME_USER/.bashrc"
# ALIAS pour su=sudo -i
[[ -e /etc/sudoers.d/$NAME_USER ]] && su $NAME_USER -c "echo -e \"alias su=\\\"sudo -i\\\"\" >> /home/$NAME_USER/.bashrc"

# # BEGIN demontage du serveur
# # On force le démontager du partage
# until ! mountpoint -q $DIR_SRV; do        
#         umount $DIR_SRV >> /dev/null 2>&1
#         sleep 0.5
# done
# # END 

# # BEGIN fill .bash_history with usefull command for starting
cat <<EOF > /root/.bash_history
systemctl stop $DM
systemctl start $DM
systemctl start $NAME_USER
systemctl stop $NAME_USER
systemctl start dux
systemctl stop dux
systemctl stop media-srv.automount
systemctl daemon-reload
systemctl poweroff
systemctl reboot
ip addr
fdisk -l
df -h
du -h -d 1 .
mount /dev/sda1 /media/tmp
systemctl status $NFSROOT
su - $NAME_USER
modprobe -a vboxdrv vboxnetadp vboxnetflt vboxpci
rmdir /var/cache/pacman/pkg/ && ln -s $DIR_SRV/packages/$ARCH/pkg/ /var/cache/pacman/
mv /var/cache/pacman/pkg/* /media/srv/packages/x64/pkg/
rm /var/cache/pacman/pkg/*
ls -l /var/cache/pacman/pkg
pacman -Syu
journalctl -fn 50
journalctl -fn 50 -u $DM
mount $DIR_SRV/
EOF
# # END .bash_history


# # BEGIN fill .bash_history with usefull command for starting
su $NAME_USER -c "touch ~/.bash_history"
cat <<EOF > /home/$NAME_USER/.bash_history  
systemctl poweroff
systemctl reboot
ip addr
df -h
du -h -d 1 .
systemctl status $NFSROOT
su - $NAME_USER
ls -l /var/cache/pacman/pkg
pacman -Syu
journalctl -fn 50
journalctl -fn 50 -u $DM
su
dbus-run-session -- startplasma-wayland --display-server --wayland
EOF
# chown $NAME_USER:users /home/$NAME_USER/.bash_history
# # END .bash_history

