# Copyright (c) Elie Coutaud 2024
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# coding: utf-8
import sys
import os
import getopt
#import lib.futil
from lib import net_conf
from lib import user_conf
from lib import graphic_env
from lib import localisation
from lib.pythcomm.futil import msg, menu, rid
import subprocess
from lib.pythcomm.helper_help import usage

import json
from lib import system_conf

f = "/tmp/test.json"
with open(f, 'w') as fp:
    json.dump(system_conf.DistConf.DEFAULT_CONF, fp, indent = 4)

if os.path.exists(f) :
    with open(f, 'rb') as f:
        dico = json.load(f)

# print(dico["ArchLinux"])

# class helper_help() :
    
usage.OPTIONS_ALIAS = {
    "a": "arch",
    "i": "interactive",
    "h": "hostname",
    "u": "username",
    "k": "keymap",
    "K": "xkbmap",
    "z": "zone",
    "n": "network",
    "g": "graphic",
    "e": "environment",
    "D": "displaymanager",
    "c": "cachedir",
    "q": "quiet",
    "t": "test",
    "S": "sudo",
    "T": "thunderbird",
    "L": "libreoffice",
    "p": "cups",
    "H": "hplip",
    "b": "bluetooth",
    # "gentoo" : "gentoo",
    # "no-sudo": "no-sudo",
    # "ram-install": "ram-install",
    # "efi" : "efi",
    "l" : "loader"
    }

usage.OPTIONS_VALUES = {
    "C": "config file",
    "a": "arch",
    "h": "hostname",
    "u": "username",
    "k": "keymap",
    "K": "xkbmap",
    "z": "zone",
    "n": "network",
    "g": "graphic",
    "e": "environment",
    "D": "displaymanager",
    "c": "cachedir",
    "l": "/dev/sdX",
    "locale": "locale",
    "luks" : "root encrypted part."
    }

usage.OPTIONS_HELP = {
    "C" : "Use an alternate config file for pacman (pacman option)",
    #"d" : "Allow installation to a non-mountpoint directory",
    "G" : "Avoid copying the host's pacman keyring to the target (pacman option)",
    "i" : "\"Interactive mode (pacman option)\"",
    "M" : "Avoid copying the host's mirrorlist to the target (pacman option)",
    "c" : "Alternative cache package directory (pacman option)",
    "locale" : "System language",
    "k": "Keyboard console keymap",
    "K": "X keymap (UI)",
    "z": "Time zone",
    "a" : "Processor architecture (x64/i386) | debian/gentoo only",
    "g" : "Graphic driver (intel,nvidia{-304,340},amdgpu,radeon,vb,all)",
    "e" : "Desktop environment (plasma,xfce,lxde,gnome,mate,cinnamon,fluxbox,enlightenment,i3)",
    "D": "Display Manager",
    "h" : "Host name",
    "u" : "Main user login",
    "S" : "Sudo use",
    # "no-sudo" : "Do not use sudo/set root password",
    "n" : "Network interface (NetworkManager/dhcpcd/dhcpcd@NETWORK_INTERFACE/...)",
    "p" : "Printers packages install (cups)",
    "H" : "HP printers packages install (hplip)",
    "b" : "Mininal bluetooth management( bluez bluez-utils )",
    "L" : "Libreoffice package install",
    "T" : "Thunderbird package install",
    "t" : "Test mode",
    "q" : "Quiet mode",
    "l" : "Grub install on /dev/sdX disk. (BIOS)",
    "efi" : "Grub install in /boot/EFI. (EFI)",
    "luks" : "LUKS partition config (GRUB)",
    # "csm" : "Secure boot config",
    "plymouth" : "Plymouth install",
    "path" : "Installation path",
    "packages" : "Additionnal packages",
    "debian" : "Debian install",
    "gentoo" : "Gentoo install (NOT TOTALLY IMPLEMENTED)",
}
usage.OPTIONS_MANDATORY = [
    "path",
]
usage.OPTIONS_ARGS = [
    "path",
    "packages",
]

usage.TEXT = "usage: python " + sys.argv[0]
usage.DESC = sys.argv[0] + " installs base packages of a distribution (Arch by default) with files in files/de/*.DIST.conf to the specified directory.\nThen generate fstab, add the user,create passwords, install grub (BIOS) if specified\nEnable systemd services like the display manager.\nAnd files/custom_user is executed as a personal script\nTest mode allow user to show all command to be run without execute."


# BEGIN Functions
def ping(host) :
    """
    Returns True if host (str) responds to a ping request.
    Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
    """

    # Option for the number of packets as a function of
    param = '-c'

    # Ping command with option quiet and one shot...
    command = ['ping', '-q', '-c', '1', host]
    ping_cmd = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT) 
    #ping_cmd = subprocess.run(command, stdin=None, input=None, stdout=subprocess.PIPE, stderr=None, shell=False, cwd=None, timeout=None, check=False, encoding=None, errors=None, env=None) 
    return ping_cmd.returncode 

# Traitement des arguments de ligne de commande
def SetConfig(argv) :
    try:
        # print(["help", *usage.GetOptsValue(),  *user_conf.user_conf.DEBIANVERSIONS])
        opts, args = getopt.getopt(argv, usage.GetOpts(), ["help", *usage.GetOptsValue(),  *user_conf.user_conf.DEBIANVERSIONS])
    except getopt.GetoptError as e: 
        message.error("{}", str(e))
        # print(e)
        usage.usage(user_conf.user_conf.DEBIANVERSIONS)
        sys.exit(2) 

    try:
        # print(str(argv) + " " + str(opts))
        for opt, arg in opts :
            # print(opt + " " +arg)
            if opt == "--help" :
                usage.usage(user_conf.user_conf.DEBIANVERSIONS)
                sys.exit(0)
            # -l is also in -libreoffice...
            elif opt in ("-l", "--loader"):
                if not os.path.exists(arg) :
                    user_input.die("Specified disk : '{}' does not exists !", arg)
                grub_disk = arg
                conf_user.GRUB_INSTALL = arg
            elif opt in ("-M"):
                global copymirrorlist
                #print(str(copymirrorlist))
                copymirrorlist = False
                #print(copymirrorlist)
            elif opt in ("-G"):
                global copykeyring
                copykeyring = False
            elif opt in ("-a", "--arch"):
                conf_user.NEW_ARCH = arg
            elif opt in ("-h", "--hostname"):
                conf_user.hostname = arg
            elif opt in ("-u", "--username"):
                conf_user.USER_NAME = arg
            elif opt in ("-S", "--sudo"):
                conf_user.SUDO = True
                conf_user.SUDO_SET = True
            # elif opt in ("--no-sudo"):
            #     conf_user.SUDO_SET = True
            elif opt in ("--locale"):
                local.LOCALE_LIB = arg
            elif opt in ("-k", "--keymap"):
                local.KBDMAP_LIB = arg
            elif opt in ("-K", "--xkbmap"):
                local.XKBDMAP_LIB = arg
            elif opt in ("-z", "--zone"):
                local.TIMEZONE_LIB = arg
            elif opt in ("-n", "--network"):
                reseau.NETCONF_LIB = arg
            elif opt in ("-g", "--graphic"):
                graphic.VID_DRIVERS_LIB = arg
            elif opt in ("-e", "--environment"):
                graphic.DESKTOP_ENV_LIB = arg
            elif opt in ("-D", "--displaymanager"):
                graphic.DISPLAYMANAGER_LIB = arg
            elif opt in ("-c", "--cachedir"):
                if os.path.exists(arg) :
                    conf_user.PACK_MANAGER_CACHE_PKG = arg
                else :
                    message.caution("Rep : {} n'existe pas !", arg)
            elif opt in ("-p", "--cups") :
                global installCups
                installCups = True
            elif opt in ("-H", "--hplip") :
                global installHPlip
                installHPlip = True
            elif opt in ("-b", "--bluetooth") :
                global installBluetooth
                installBluetooth = True
            elif opt in ("-T", "--thunderbird") :
                global installMail
                installMail = True
            elif opt in ("-L", "--libreoffice") :
                global installOffice
                installOffice= True
            elif opt in ("-q", "--quiet"):
                conf_user.QUIET = True
            elif opt in ("-t", "--test"):
                conf_user.TEST_MODE = True
            elif opt in ("-i", "--interactive") :
                conf_user.INTERACTIVE = True
            elif opt.replace("--", "") in user_conf.user_conf.DEBIANVERSIONS:
                conf_user.DEBIAN_VERS = opt.replace("--", "")
                conf_user.DISTRIB = "debian|" + conf_user.DEBIAN_VERS
            elif opt in ("--gentoo"):
                conf_user.DISTRIB = "gentoo"
            elif opt in ("--debian"):
                conf_user.DISTRIB = "debian"
            elif opt in ("--ram-install"):
                conf_user.RAM_INSTALL = True
            elif opt in ("--plymouth"):
                conf_user.PLYMOUTH = True
            elif opt in ("--efi"):
                conf_user.GRUB_EFI = True
            elif opt in ("--luks"):
                if os.path.exists(arg) :
                    conf_user.LUKS_PART = arg
                else :
                    user_input.error("{} does not exists !", arg)
                    sys.exit(2)
        # print(args)
        for arg in args :
            if os.path.exists(arg) and os.path.isdir(arg) :
                conf_user.PATH_INSTALL = os.path.realpath(arg)
            else :
                # print(arg)
                conf_user.PACKAGES += " " + arg
    except Exception as e :
        message.error(str(e))
# END Functions

try :
    copykeyring = True
    copymirrorlist = True
    local = localisation.Localisation()
    graphic = graphic_env.graphic_env()
    reseau = net_conf.NetworkConfiguration()
    conf_user = user_conf.user_conf(local, graphic, reseau)
    installCups = False
    installHPlip = False
    installBluetooth = False
    installMail = False
    installOffice = False
    grub_disk = ""
    message = msg()
    message.COLORED_PROMPT = True
    user_input = rid(message)
    
    SetConfig(sys.argv[1:])


    if conf_user.PATH_INSTALL == "" or conf_user.PATH_INSTALL == "/" :
        user_input.error("Aucun répertoire d'installation spécifié {} !", conf_user.PATH_INSTALL)
        usage.usage(sys.argv[1:])
        sys.exit(1)

    if conf_user.DISTRIB.split("|")[0] == "debian" or conf_user.DISTRIB.startswith("debian") :
        from lib import debian_conf
        conf_user = debian_conf.debian_conf(user_input, conf_user, local, graphic, reseau)
        distrib = conf_user.DISTRIB + "|" + conf_user.DEBIAN_VERS
    elif conf_user.DISTRIB.startswith("gentoo") :
        from lib import gentoo_conf
        conf_user = gentoo_conf.gentoo_conf(user_input, conf_user, local, graphic, reseau, copymirrorlist, copykeyring)
        distrib = conf_user.DISTRIB
    else :
        from lib import arch_conf
        conf_user = arch_conf.arch_conf(user_input, conf_user, local, graphic, reseau, copymirrorlist, copykeyring)
        distrib = conf_user.DISTRIB
        
    # print(conf_user.PLYMOUTH)
    txt="Welcome to {} (dev)"
    message.msg_n(txt, "32", "32", "BYOD")
    message.msg_n2("Installing {}", "32", "32",  distrib)

    conf_user.UUID_ROOT = conf_user.GetDiskUUID()
    # Architecture (debian/gentoo)
    conf_user.SetArch(user_input, conf_user.NEW_ARCH)
    # Graphic driver
    conf_user.SetGraphicDriver(user_input, graphic.VID_DRIVERS_LIB)
    # Desktop Environnement
    conf_user.SetEnvironnement(user_input, graphic.DESKTOP_ENV_LIB)
    # Network configuration
    conf_user.SetNetworkConfig(user_input, reseau.NETCONF_LIB)
    # User settings
    conf_user.SetUser(user_input, conf_user.USER_NAME)
    # Hostname
    conf_user.SetHostname(user_input, conf_user.hostname)
    # Wait image debian/arch download for next steps
    if not conf_user.WaitForDownload(False, "Downloading {}...", conf_user.DISTRIB) :
        sys.exit(1)
    # Locales
    conf_user.SetLocale(user_input, local.LOCALE_LIB)
    # Time zone
    conf_user.SetTimeZone(user_input, local.TIMEZONE_LIB)
    # Keymaps
    conf_user.SetKeymap(user_input, local.KBDMAP_LIB)
    # Keymaps sous X
    conf_user.SetXKbdMap(user_input, local.XKBDMAP_LIB)
    # Additionnal usefull packages
    conf_user.SetAdditionnalPackage(installBluetooth, installHPlip, installCups, installOffice, installMail)
    # Continue only as root :
    if not conf_user.TEST_MODE and not os.geteuid() == 0  :
        user_input.error("Only {} can continue : ", "root")
        user_input.msg_nn("Run the following command to avoid questions : \n{}", "sudo")
        print(sys.argv[0] + conf_user.next_cmd())
    else :
        # Install new linux distribution, top !
        if user_input.rid_continue("Install {} ?", "32", "32", distrib) :
            conf_user.install()
    
except KeyboardInterrupt as e :
    user_input.out_n("\n==>", "{}", "Interruption !" ,"31", "31" )
    print()

except Exception as e:
    message.error(str(e))
    # if DEBUG :
    #     if e.errno == os.errno.ENOENT:
    #         sys.exit(e.errno)
finally :
    if conf_user.TEST_MODE :
        if len(conf_user.LIST_CMD) > 0 :
            if user_input.rid_continue("{}", "CMD ") :
                for cmd in conf_user.LIST_CMD :
                    # print(str(repr(cmd))[1:len(repr(cmd)) - 1])
                    print(str(cmd))
    #if os.geteuid() == 0  :
    print("\npython " + sys.argv[0] + conf_user.next_cmd()) #, reseau))
    # user_input.msg_n("{}", "32", "32", "End" )
#return True
